package se.plushogskolan.jetbroker.plane.integration.jetbroker;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

    @Inject
    private Logger log;

    @Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
    private TopicConnectionFactory connectionFactory;

    // Lookup Topic
    @Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
    private Topic topic;

    @Override
    public void broadcastNewFuelPrice (double fuelPrice) {
        log.fine("PROD: broadcastNewFuelPrice. New price is " + fuelPrice);

        TopicConnection connection = null;
        TopicSession session = null;
        try {
            connection = connectionFactory.createTopicConnection();
            session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            Message message = session.createMessage();
            message.setDoubleProperty("fuelPrice", fuelPrice);
            message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);

            TopicPublisher publisher = session.createPublisher(topic);
            publisher.publish(message);
        }
        catch (JMSException e) {
            throw new FailedIntegrationConnectionException(e.getMessage(), e);
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }
    }

    @Override
    public void broadcastAirportsChanged () {
        log.fine("PROD: Airports changed");
        broadcastChanged(ChangedType.AIRPORT);
    }

    @Override
    public void broadcastPlaneTypesChanged () {
        log.fine("PROD: Plane types changed");
        broadcastChanged(ChangedType.PLANE_TYPE);
    }

    private void broadcastChanged (ChangedType type) {
        TopicConnection connection = null;
        TopicSession session = null;
        try {
            connection = connectionFactory.createTopicConnection();
            session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            Message message = session.createMessage();
            message.setStringProperty("messageType", type.jmsMessageConstant);

            TopicPublisher publisher = session.createPublisher(topic);
            publisher.publish(message);
        }
        catch (JMSException e) {
            throw new FailedIntegrationConnectionException(e.getMessage(), e);
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }
    }

    private enum ChangedType {
        AIRPORT(JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED),
        PLANE_TYPE(JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED);

        String jmsMessageConstant;

        ChangedType (String jmsMessageConstant) {
            this.jmsMessageConstant = jmsMessageConstant;
        }
    }


}
