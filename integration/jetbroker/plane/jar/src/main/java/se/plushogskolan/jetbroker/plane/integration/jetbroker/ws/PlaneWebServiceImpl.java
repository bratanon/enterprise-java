package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
@WebService(endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService", name = "PlaneWebService")
@Stateless
public class PlaneWebServiceImpl implements PlaneWebService {

    @Inject
    Logger log;

    @Inject
    private AirportService airportService;

    @Inject
    private PlaneTypeService planeTypeService;

    @Override
    public List<WSAirport> getAirports () {
        log.fine("Plane WS - get airports");
        final List<Airport> allAirports = airportService.getAllAirports();

        List<WSAirport> airportsToSend = new ArrayList<WSAirport>();
        for (Airport airport : allAirports) {
            airportsToSend.add(convertAirportToWsAirport(airport));
        }

        return airportsToSend;
    }

    @Override
    public List<WSPlaneType> getPlaneTypes () {
        log.fine("Plane WS - Get plane types");
        final List<PlaneType> allPlaneTypes = planeTypeService.getAllPlaneTypes();

        List<WSPlaneType> planeTypesToSend = new ArrayList<WSPlaneType>();
        for (PlaneType planeType : allPlaneTypes) {
            planeTypesToSend.add(convertPlaneTypeToWsPlaneType(planeType));
        }

        return planeTypesToSend;
    }

    private WSAirport convertAirportToWsAirport(Airport airport) {
        WSAirport wsAirport = new WSAirport();
        wsAirport.setCode(airport.getCode());
        wsAirport.setName(airport.getName());
        wsAirport.setLatitude(airport.getLatitude());
        wsAirport.setLongitude(airport.getLongitude());

        return wsAirport;
    }

    private WSPlaneType convertPlaneTypeToWsPlaneType(PlaneType planeType) {
        WSPlaneType wsPlaneType = new WSPlaneType();
        wsPlaneType.setCode(planeType.getCode());
        wsPlaneType.setName(planeType.getName());
        wsPlaneType.setMaxNoOfPassengers(planeType.getMaxNoOfPassengers());
        wsPlaneType.setMaxRangeKm(planeType.getMaxRangeKm());
        wsPlaneType.setMaxSpeedKmH(planeType.getMaxSpeedKmH());
        wsPlaneType.setFuelConsumptionPerKm(planeType.getFuelConsumptionPerKm());

        return wsPlaneType;
    }
}
