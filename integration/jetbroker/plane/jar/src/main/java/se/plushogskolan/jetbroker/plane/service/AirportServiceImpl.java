package se.plushogskolan.jetbroker.plane.service;

import org.joda.time.DateTime;
import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.airportws.AirportWebservice;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class AirportServiceImpl implements AirportService {

    @Inject
    Logger log;

    @Inject
    private AirportRepository airportRepo;

    @Inject
    private PlaneIntegrationFacade planeIntegrationFacade;

    @Inject
    private AirportWebservice airportWebservice;

    @Override
    public Airport getAirport (long id) {
        return getAirportRepo().findById(id);
    }

    @Override
    public Airport getAirportByCode (String code) {
        return getAirportRepo().getAirportByCode(code);
    }

    @Override
    public void updateAirport (Airport airport) {
        log.fine("Update airport : " + airport);
        getAirportRepo().update(airport);
        planeIntegrationFacade.broadcastAirportsChanged();
    }

    @Override
    public Airport createAirport (Airport airport) {
        log.fine("Creating airport : " + airport);
        long id = getAirportRepo().persist(airport);
        planeIntegrationFacade.broadcastAirportsChanged();
        return getAirport(id);
    }

    @Override
    public List<Airport> getAllAirports () {
        List<Airport> airports = getAirportRepo().getAllAirports();
        Collections.sort(airports);
        return airports;
    }

    @Override
    public void deleteAirport (long id) {
        log.fine("Delete airport with id : " + id);
        Airport airport = getAirport(id);
        getAirportRepo().remove(airport);
        planeIntegrationFacade.broadcastAirportsChanged();
    }

    @Override
    public void updateAirportsFromWebService () {
        log.fine("Update airports from external webservice");
        Collection<Airport> allAirports;
        DateTime date = new DateTime().now();

        try {
            allAirports = airportWebservice.getAllAirports();
        }
        catch (FailedIntegrationConnectionException e) {
            log.severe("FailedIntegrationConnectionException");
            return;
        }

        for (Airport airport : allAirports) {
            Airport airportByCode = getAirportByCode(airport.getCode());

            if (airportByCode == null) {
                airport.setImported(true);
                getAirportRepo().persist(airport);
            }
            else {
                airport.setId(airportByCode.getId());
                airport.setImported(true);
                getAirportRepo().update(airport);
            }
        }

        getAirportRepo().deleteAirportsOlderThan(date);

        planeIntegrationFacade.broadcastAirportsChanged();
    }

    public AirportRepository getAirportRepo () {
        return airportRepo;
    }

    public void setAirportRepo (AirportRepository airportRepo) {
        this.airportRepo = airportRepo;
    }

    public PlaneIntegrationFacade getPlaneIntegrationFacade () {
        return planeIntegrationFacade;
    }

    public void setPlaneIntegrationFacade (PlaneIntegrationFacade planeIntegrationFacade) {
        this.planeIntegrationFacade = planeIntegrationFacade;
    }
}
