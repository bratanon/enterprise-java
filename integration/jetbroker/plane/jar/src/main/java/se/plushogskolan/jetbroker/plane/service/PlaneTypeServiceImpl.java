package se.plushogskolan.jetbroker.plane.service;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

    @Inject
    private Logger log;

    @Inject
    private PlaneTypeRepository planeTypeRepository;

    @Inject
    private PlaneIntegrationFacade planeIntegrationFacade;

    @Override
    public PlaneType getPlaneType (long id) {
        return getPlaneTypeRepository().findById(id);
    }

    @Override
    public PlaneType createPlaneType (PlaneType planeType) {
        log.fine("Create plane type : " + planeType);
        long id = getPlaneTypeRepository().persist(planeType);
        planeIntegrationFacade.broadcastPlaneTypesChanged();
        return getPlaneType(id);
    }

    @Override
    public void updatePlaneType (PlaneType planeType) {
        log.fine("Updating plane type : " + planeType);
        getPlaneTypeRepository().update(planeType);
        planeIntegrationFacade.broadcastPlaneTypesChanged();
    }

    @Override
    public List<PlaneType> getAllPlaneTypes () {
        return getPlaneTypeRepository().getAllPlaneTypes();
    }
    @Override
    public void deletePlaneType (long id) {
        log.fine("Delete plane type with id : " + id);
        PlaneType planeType = getPlaneType(id);
        getPlaneTypeRepository().remove(planeType);
        planeIntegrationFacade.broadcastPlaneTypesChanged();
    }

    public PlaneTypeRepository getPlaneTypeRepository () {
        return planeTypeRepository;
    }

    public void setPlaneRepository (PlaneTypeRepository planeTypeRepository) {
        this.planeTypeRepository = planeTypeRepository;
    }

    public PlaneIntegrationFacade getPlaneIntegrationFacade () {
        return planeIntegrationFacade;
    }

    public void setPlaneIntegrationFacade (PlaneIntegrationFacade planeIntegrationFacade) {
        this.planeIntegrationFacade = planeIntegrationFacade;
    }
}
