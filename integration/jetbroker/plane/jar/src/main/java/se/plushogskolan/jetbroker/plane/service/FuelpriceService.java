package se.plushogskolan.jetbroker.plane.service;

import javax.ejb.Lock;
import javax.ejb.LockType;

public interface FuelpriceService {

    @Lock(LockType.READ)
    public double getFuelCost();

    @Lock(LockType.WRITE)
    public void updateFuelCost(double fuelCost);
}
