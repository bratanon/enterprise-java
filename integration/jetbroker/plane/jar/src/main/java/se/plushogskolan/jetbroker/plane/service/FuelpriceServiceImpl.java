package se.plushogskolan.jetbroker.plane.service;

import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.logging.Logger;

@Singleton
@Startup
public class FuelpriceServiceImpl implements FuelpriceService {

    @Inject
    private Logger log;

    @Inject
    private PlaneIntegrationFacade planeIntegrationFacade;

    private static double cachedFuelCost = 5.2;

    @Override
    public double getFuelCost() {
        return cachedFuelCost;
    }

    @Override
    public void updateFuelCost(double fuelCost) {
        cachedFuelCost = fuelCost;

        planeIntegrationFacade.broadcastNewFuelPrice(cachedFuelCost);
    }

}
