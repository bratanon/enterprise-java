package se.plushogskolan.jetbroker.plane.integration.jetbroker.mock;

import se.plushogskolan.jee.utils.cdi.MockIntegration;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

import javax.inject.Inject;
import java.util.logging.Logger;

@MockIntegration
public class PlaneIntegrationFacadeMock implements PlaneIntegrationFacade {

    @Inject
    Logger log;

    @Override
    public void broadcastNewFuelPrice (double fuelPrice) {
        log.info("MOCK: broadcastNewFuelPrice. New price is " + fuelPrice);
    }

    @Override
    public void broadcastAirportsChanged () {
        log.info("MOCK: Airports changed");
    }

    @Override
    public void broadcastPlaneTypesChanged () {
        log.info("MOCK: Plane types changed");

    }

}
