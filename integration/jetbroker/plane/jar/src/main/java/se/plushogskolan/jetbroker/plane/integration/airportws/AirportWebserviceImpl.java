package se.plushogskolan.jetbroker.plane.integration.airportws;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.airportws.stubs.AirportSoap;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
public class AirportWebserviceImpl implements AirportWebservice {

    @Inject
    private Logger log;

    @Override
    public Collection<Airport> getAllAirports () throws FailedIntegrationConnectionException {
        log.fine("In AirportWebserviceImpl::getAllAirports");
        String airportData;
        try {
            final se.plushogskolan.jetbroker.plane.integration.airportws.stubs.Airport service = new se.plushogskolan.jetbroker.plane.integration.airportws.stubs.Airport();
            final AirportSoap port = service.getAirportSoap();
            airportData = port.getAirportInformationByCountry("Sweden");
        }
        catch (WebServiceException e) {
            throw new FailedIntegrationConnectionException(e.getMessage());
        }

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            AirportDataSetHandler handler = new AirportDataSetHandler();

            InputSource inputSource = new InputSource(new StringReader(airportData));
            saxParser.parse(inputSource, handler);
            return handler.getAirports();
        }
        catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static class AirportDataSetHandler extends DefaultHandler {

        private Collection<Airport> airports;
        private Airport currentAirport;
        private String currentTextValue;

        public Collection<Airport> getAirports () {
            return airports;
        }

        @Override
        public void startDocument () throws SAXException {
            airports = new HashSet<>();
        }

        @Override
        public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.toLowerCase().equals("table")) {
                currentAirport = new Airport();
            }
        }

        @Override
        public void endElement (String uri, String localName, String qName) throws SAXException {
            switch (qName.toLowerCase()) {
                case "airportcode":
                    currentAirport.setCode(currentTextValue);
                    break;
                case "cityorairportname":
                    currentAirport.setName(currentTextValue);
                    break;
                case "latitudedegree":
                    currentAirport.setLatitude(Double.parseDouble(currentTextValue));
                    break;
                case "latitudeminute":
                    currentAirport.setLatitude(currentAirport.getLatitude() + (Double.parseDouble(currentTextValue) / 60));
                    break;
                case "latitudesecond":
                    currentAirport.setLatitude(currentAirport.getLatitude() + (Double.parseDouble(currentTextValue) / 3600));
                    break;
                case "latitudenpeers":
                    if (currentTextValue.equals("S")) {
                        currentAirport.setLatitude(currentAirport.getLatitude() * -1);
                    }
                    break;
                case "longitudedegree":
                    currentAirport.setLongitude(Double.parseDouble(currentTextValue));
                    break;
                case "longitudeminute":
                    currentAirport.setLongitude(currentAirport.getLongitude() + (Double.parseDouble(currentTextValue) / 60));
                    break;
                case "longitudeseconds":
                    currentAirport.setLongitude(currentAirport.getLongitude() + (Double.parseDouble(currentTextValue) / 3600));
                    break;
                case "longitudeeperw":
                    if (currentTextValue.equals("W")) {
                        currentAirport.setLongitude(currentAirport.getLongitude() * -1);
                    }
                    break;
                case "table":
                    airports.add(currentAirport);
                    break;
            }
        }

        @Override
        public void characters (char[] ch, int start, int length) throws SAXException {
            currentTextValue = new String(ch, start, length);
        }
    }
}
