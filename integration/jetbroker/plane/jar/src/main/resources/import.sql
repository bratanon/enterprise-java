-- You can use this file to load seed data into the database using SQL statements
INSERT INTO `jetbroker_plane`.`airport` (`code`, `lastUpdated`, `latitude`, `longitude`, `name`) VALUES ('AAA', '2014-10-23 03:48:54', '10', '20', 'AAA Airport');
INSERT INTO `jetbroker_plane`.`airport` (`code`, `lastUpdated`, `latitude`, `longitude`, `name`) VALUES ('BBB', '2014-10-23 03:48:54', '30', '80', 'BBB Airport');
