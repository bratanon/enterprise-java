package se.plushogskolan.jetbroker.plane.mvc;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.FuelpriceService;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

@Controller
public class IndexController {

    @Inject
    private PlaneTypeService planeTypeService;

    @Inject
    private AirportService airportService;

    @Inject
    private FuelpriceService fuelPriceService;

    @RequestMapping("/index.html")
    public ModelAndView index() {

        List<PlaneType> planeTypes = getPlaneTypeService().getAllPlaneTypes();
        List<Airport> airports = getAirportService().getAllAirports();
        double fuelCost = getFuelPriceService().getFuelCost();

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("planeTypes", planeTypes);
        mav.addObject("airports", airports);
        mav.addObject("fuelCost", fuelCost);
        return mav;
    }

    @RequestMapping("/updateAirports.html")
    public String updateAirports() {
        getAirportService().updateAirportsFromWebService();
        return "redirect:/index.html";
    }

    public PlaneTypeService getPlaneTypeService() {
        return planeTypeService;
    }

    public void setPlaneTypeService(PlaneTypeService planeTypeService) {
        this.planeTypeService = planeTypeService;
    }

    public AirportService getAirportService() {
        return airportService;
    }

    public void setAirportService(AirportService airportService) {
        this.airportService = airportService;
    }

    public FuelpriceService getFuelPriceService() {
        return fuelPriceService;
    }

    public void setFuelPriceService(FuelpriceService fuelPriceService) {
        this.fuelPriceService = fuelPriceService;
    }

}
