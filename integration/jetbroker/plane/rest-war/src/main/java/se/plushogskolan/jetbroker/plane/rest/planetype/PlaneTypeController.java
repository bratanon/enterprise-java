package se.plushogskolan.jetbroker.plane.rest.planetype;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeRequest;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

@RestController
public class PlaneTypeController {

    Logger log = Logger.getLogger(PlaneTypeController.class.getName());

    @Inject
    private PlaneTypeService planeTypeService;

    @RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<PlaneType> createPlaneType(@RequestBody CreatePlaneTypeRequest request) {
        PlaneType planeType = null;
        try {
            planeType = planeTypeService.createPlaneType(request.buildPlaneType());
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<PlaneType> getPlaneType(@PathVariable long id) {
        PlaneType planeType = null;
        try {
            planeType = planeTypeService.getPlaneType(id);
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<PlaneType> deletePlaneType(@PathVariable long id) {
        PlaneType planeType = planeTypeService.getPlaneType(id);

        if (planeType == null) {
            return new ResponseEntity<PlaneType>(HttpStatus.NOT_FOUND);
        }

        try {
            planeTypeService.deletePlaneType(planeType.getId());
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<PlaneType>(planeType, HttpStatus.BAD_REQUEST);
        }
    }
}
