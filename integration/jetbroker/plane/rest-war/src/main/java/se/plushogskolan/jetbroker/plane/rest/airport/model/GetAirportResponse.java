package se.plushogskolan.jetbroker.plane.rest.airport.model;

import se.plushogskolan.jetbroker.plane.domain.Airport;

public class GetAirportResponse {

	private long id;
	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public GetAirportResponse(Airport airport) {
		setId(airport.getId());
		setCode(airport.getCode());
		setName(airport.getName());
		setLatitude(airport.getLatitude());
		setLongitude(airport.getLongitude());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "GetAirportResponse [id=" + id + ", code=" + code + ", name=" + name + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}

}
