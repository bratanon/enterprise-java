package se.plushogskolan.jetbroker.plane.rest.airport;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportRequest;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.FuelpriceService;

@RestController
public class AirportController {

    Logger log = Logger.getLogger(AirportController.class.getName());

    @Inject
    private AirportService airportService;

    @Inject
    private FuelpriceService fuelpriceService;

    @RequestMapping(value = "/createAirport", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Airport> createAirport(@RequestBody CreateAirportRequest request) throws Exception {
        Airport airport = null;
        try {
            airport = airportService.createAirport(request.buildAirport());
            return new ResponseEntity<Airport>(airport, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Airport>(airport, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getAirport/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Airport> getAirport(@PathVariable long id) {
        Airport airport = null;
        try {
            airport = airportService.getAirport(id);
            return new ResponseEntity<Airport>(airport, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Airport>(airport, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/deleteAirport/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Airport> deleteAirport(@PathVariable long id) {
        Airport airport = airportService.getAirport(id);

        if (airport == null) {
            return new ResponseEntity<Airport>(HttpStatus.NOT_FOUND);
        }

        try {
            airportService.deleteAirport(id);
            return new ResponseEntity<Airport>(airport, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Airport>(airport, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/updateFuelPrice/{fuelPrice}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateFuelPrice(@PathVariable double fuelPrice) {
        try {
            fuelpriceService.updateFuelCost(fuelPrice);
            return new ResponseEntity<String>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

    }

}
