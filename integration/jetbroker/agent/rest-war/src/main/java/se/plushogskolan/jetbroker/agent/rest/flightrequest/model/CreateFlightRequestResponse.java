package se.plushogskolan.jetbroker.agent.rest.flightrequest.model;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class CreateFlightRequestResponse {

	private long id;

	public CreateFlightRequestResponse(FlightRequest flightRequest) {

		setId(flightRequest.getId());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CreateFlightRequestResponse [id=" + id + "]";
	}

}
