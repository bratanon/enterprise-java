package se.plushogskolan.jetbroker.agent.rest.customer;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.rest.customer.model.CreateCustomerRequest;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

@RestController
public class CustomerController {

    @Inject
    private CustomerService customerService;

    @RequestMapping(value = "createCustomer", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Customer> createPlane(@RequestBody CreateCustomerRequest request) {
        Customer customer = null;
        HttpStatus statusCode = HttpStatus.CREATED;
        try {
            customer = customerService.createCustomer(request.buildCustomer());
        }
        catch (Exception e) {
            statusCode = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<Customer>(customer, statusCode);
    }

    @RequestMapping(value = "getCustomer/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Customer> getPlane(@PathVariable long id) {
        Customer customer = customerService.getCustomer(id);
        HttpStatus statusCode = HttpStatus.OK;
        if (customer == null) {
            statusCode = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<Customer>(customer, statusCode);
    }

    @RequestMapping(value = "deleteCustomer/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Customer> deletePlane(@PathVariable long id) {
        Customer customer = customerService.getCustomer(id);
        HttpStatus statusCode = HttpStatus.OK;

        if (customer == null) {
            statusCode = HttpStatus.NOT_FOUND;
        }

        try {
            customerService.deleteCustomer(customer.getId());
        }
        catch (Exception e) {
            customer = null;
            statusCode = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<Customer>(customer, statusCode);
    }
}
