package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

    public static String buildXmlUsingDom(FlightRequest fr) throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.newDocument();

        Element root = doc.createElement("flightrequest");
        root.setAttribute("id", String.valueOf(fr.getId()));
        doc.appendChild(root);

        Element arrivalAirportCode = doc.createElement("arrivalAirportCode");
        root.appendChild(arrivalAirportCode);
        arrivalAirportCode.setTextContent(fr.getArrivalAirportCode());

        Element departureAriportCode = doc.createElement("departureAirportCode");
        root.appendChild(departureAriportCode);
        departureAriportCode.setTextContent(fr.getDepartureAirportCode());

        Element noOfPassengers = doc.createElement("noOfPassengers");
        root.appendChild(noOfPassengers);
        noOfPassengers.setTextContent(String.valueOf(fr.getNoOfPassengers()));

        Element status = doc.createElement("status");
        root.appendChild(status);
        status.setTextContent(fr.getRequestStatus().toString());

        return XmlUtil.convertXmlDocumentToString(doc);
    }
}
