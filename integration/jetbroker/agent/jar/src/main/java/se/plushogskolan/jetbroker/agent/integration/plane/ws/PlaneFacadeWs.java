package se.plushogskolan.jetbroker.agent.integration.plane.ws;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
public class PlaneFacadeWs implements PlaneFacade {

    @Inject
    Logger log;

    @Override
    public List<AirPort> getAllAirports () {
        log.fine("In PlaneFacadeWs::getAllAirports");

        final PlaneWebServiceImplService service = new PlaneWebServiceImplService();
        final PlaneWebService port = service.getPlaneWebServicePort();

        final List<WsAirport> airportsFromWs = port.getAirports();

        final List<AirPort> airports = new ArrayList<AirPort>();
        for (WsAirport wsAirport : airportsFromWs) {
            airports.add(convertWsAirportToAirport(wsAirport));
        }

        return airports;
    }

    @Override
    public List<PlaneType> getAllPlaneTypes () {
        log.fine("In PlaneFacadeWs::getAllPlaneTypes");

        final PlaneWebServiceImplService service = new PlaneWebServiceImplService();
        final PlaneWebService port = service.getPlaneWebServicePort();

        final List<WsPlaneType> planeTypesFromWs = port.getPlaneTypes();

        final List<PlaneType> planeTypes = new ArrayList<PlaneType>();
        for (WsPlaneType wsPlaneType : planeTypesFromWs) {
            planeTypes.add(convertWsPlaneTypeToPlaneType(wsPlaneType));
        }

        return planeTypes;
    }

    private AirPort convertWsAirportToAirport(WsAirport wsAirport) {
        AirPort airport = new AirPort();
        airport.setCode(wsAirport.getCode());
        airport.setName(wsAirport.getName());

        return airport;
    }

    private PlaneType convertWsPlaneTypeToPlaneType(WsPlaneType wsPlaneType) {
        PlaneType planeType = new PlaneType();
        planeType.setCode(wsPlaneType.getCode());
        planeType.setName(wsPlaneType.getName());
        planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
        planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
        planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
        planeType.setFuelConsumptionPerKm(wsPlaneType.getFuelConsumptionPerKm());

        return planeType;
    }
}
