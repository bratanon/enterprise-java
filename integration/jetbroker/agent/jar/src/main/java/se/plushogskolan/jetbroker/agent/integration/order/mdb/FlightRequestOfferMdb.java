package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"
        ),
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE
        ),
        @ActivationConfigProperty(
                propertyName = "messageSelector",
                propertyValue = "messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_OFFER + "'"
        )
})
public class FlightRequestOfferMdb extends AbstractMDB implements MessageListener {

    @Inject
    private Logger log;

    @Inject
    private FlightRequestService flightRequestService;

    @Override
    public void onMessage(Message message) {
        log.info("Incoming FlightRequestOffer.");
        try {
            TextMessage msg = (TextMessage) message;

            XmlFlightRequestOffer xmlOffer = (XmlFlightRequestOffer) JaxbUtil.generateJaxbObject(msg.getText(), XmlFlightRequestOffer.class);

            FlightOffer flightOffer = new FlightOffer();
            flightOffer.setOfferedPrice(xmlOffer.getOfferedPrice());
            flightOffer.setPlaneTypeCode(xmlOffer.getOfferedPlaneTypeCode());

            flightRequestService.handleFlightOffer(flightOffer, xmlOffer.getAgentId());
        }
        catch (Exception e) {
            throw new FailedIntegrationConnectionException(e.getMessage());
        }

    }

}
