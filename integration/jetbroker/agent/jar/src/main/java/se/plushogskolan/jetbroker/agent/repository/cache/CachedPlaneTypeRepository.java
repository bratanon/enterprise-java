package se.plushogskolan.jetbroker.agent.repository.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {

    private final static String ALL_PLANETYPES_KEY = "ALL_PLANETYPES";

    @Inject
    Logger log;

    @Inject
    private PlaneFacade planeFacade;

    @Override
    public List<PlaneType> getAllPlaneTypes () {
        log.fine("Get all plane types.");

        Cache cache = CacheManager.getInstance().getCache("planeTypeCache");
        if (!cache.isKeyInCache(ALL_PLANETYPES_KEY)) {
            log.fine("No plane types found in the cache.");
            log.fine("Populate the cache with the plane types from the SOAP call.");
            log.fine("Returns fresh plane types from the SOAP call.");
            final List<PlaneType> allPlaneTypes = planeFacade.getAllPlaneTypes();
            cache.put(new Element(ALL_PLANETYPES_KEY, allPlaneTypes));
            return allPlaneTypes;
        }

        log.fine("Get all plane types returns the cached values.");
        Element element = cache.get(ALL_PLANETYPES_KEY);
        return (List<PlaneType>) element.getObjectValue();
    }

    @Override
    public PlaneType getPlaneType (String code) {
        log.fine("Get plane type with code " + code);

        Cache cache = CacheManager.getInstance().getCache("planeTypeCache");
        if (!cache.isKeyInCache(code)) {
            final List<PlaneType> allAirPorts = getAllPlaneTypes();
            for (PlaneType planeType : allAirPorts) {
                if (planeType.getCode().equals(code)) {
                    log.fine("Plane type with code " + code + " saved in the cache.");
                    cache.put(new Element(code, planeType));
                    return planeType;
                }
            }
            log.info("No plane type found for code " + code);
            return null;
        }

        log.info("Plane type " + code + " found in cache");
        Element element = cache.get(code);
        return (PlaneType) element.getObjectValue();
    }

    @Override
    public void handlePlaneTypeChangedEvent () {
        log.fine("Handle plane types changed event");
        log.fine("Clearing plane type cache");
        Cache cache = CacheManager.getInstance().getCache("planeTypeCache");
        cache.removeAll();
    }

}
