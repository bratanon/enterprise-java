package se.plushogskolan.jetbroker.agent.integration.plane.mdb;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.PlaneTypeService;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Topic"
        ),
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST
        ),
        @ActivationConfigProperty(
                propertyName = "messageSelector",
                propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED + "'"
        )
})
public class PlaneTypesChangedMdb extends AbstractMDB implements MessageListener {

    @Inject
    private Logger log;

    @Inject
    private PlaneTypeService planeTypeService;

    @Override
    public void onMessage (Message message) {
        log.fine("Incoming PlaneTypesChangedMdb.");
        planeTypeService.handlePlaneTypesChangedEvent();
    }
}
