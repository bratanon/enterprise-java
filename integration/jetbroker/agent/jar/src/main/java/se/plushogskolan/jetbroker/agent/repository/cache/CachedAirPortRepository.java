package se.plushogskolan.jetbroker.agent.repository.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

    private final static String ALL_AIRPORTS_KEY = "ALL_AIRPORTS";

    @Inject
    Logger log;

    @Inject
    private PlaneFacade planeFacade;

    @Override
    public List<AirPort> getAllAirPorts () {
        log.fine("Get all airports.");

        Cache cache = CacheManager.getInstance().getCache("airportCache");
        if (!cache.isKeyInCache(ALL_AIRPORTS_KEY)) {
            log.fine("No airports found in the cache.");
            log.fine("Populate the cache with the airports from the SOAP call.");
            log.fine("Returns fresh airports from the SOAP call.");
            final List<AirPort> allAirports = planeFacade.getAllAirports();
            cache.put(new Element(ALL_AIRPORTS_KEY, allAirports));
            return allAirports;
        }

        log.fine("Get all airports returns the cached values.");
        Element element = cache.get(ALL_AIRPORTS_KEY);
        return (List<AirPort>) element.getObjectValue();
    }

    @Override
    public AirPort getAirPort (String code) {
        log.fine("Get airport with code " + code);

        Cache cache = CacheManager.getInstance().getCache("airportCache");
        if (!cache.isKeyInCache(code)) {
            final List<AirPort> allAirPorts = getAllAirPorts();
            for (AirPort airPort : allAirPorts) {
                if (airPort.getCode().equals(code)) {
                    log.fine("Airport with code " + code + " saved in the cache.");
                    cache.put(new Element(code, airPort));
                    return airPort;
                }
            }
            log.info("No airport found for code " + code);
            return null;
        }

        log.info("Airport " + code + " found in cache");
        Element element = cache.get(code);
        return (AirPort) element.getObjectValue();
    }

    @Override
    public void handleAirportsChangedEvent () {
        log.fine("Received airports changed event.");
        log.fine("Clearing airport cache.");
        Cache cache = CacheManager.getInstance().getCache("airportCache");
        cache.removeAll();
    }

}
