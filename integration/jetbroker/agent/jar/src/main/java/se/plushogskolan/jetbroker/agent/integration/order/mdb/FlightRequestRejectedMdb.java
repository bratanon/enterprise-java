package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"
        ),
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE
        ),
        @ActivationConfigProperty(
                propertyName = "messageSelector",
                propertyValue = "messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_REJECTION + "'"
        )
})
public class FlightRequestRejectedMdb extends AbstractMDB implements MessageListener {

    @Inject
    private Logger log;

    @Inject
    private FlightRequestService flightRequestService;

    @Override
    public void onMessage(Message message) {
        log.info("Incomming FlightRequestRejected.");
        try {
            MapMessage msg = (MapMessage) message;
            flightRequestService.handleFlightRequestRejection(msg.getLong("agentId"));
        }
        catch (JMSException e) {
            throw new FailedIntegrationConnectionException(e.getMessage());
        }
    }
}
