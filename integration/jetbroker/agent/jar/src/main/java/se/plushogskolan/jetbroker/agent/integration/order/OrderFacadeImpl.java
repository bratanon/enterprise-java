package se.plushogskolan.jetbroker.agent.integration.order;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

@Prod
public class OrderFacadeImpl implements OrderFacade {

    @Inject
    private Logger log;

    @Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
    private QueueConnectionFactory connectionFactory;

    @Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
    private Queue queue;

    @Override
    public void sendFlightRequest(FlightRequest request) throws Exception {
        log.fine("sendFlightRequest in OrderFacade PROD: " + request);
        QueueConnection connection = null;
        QueueSession session = null;

        try {
            connection = connectionFactory.createQueueConnection();
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            connection.start();

            ObjectFactory objectFactory = new ObjectFactory();
            XmlFlightRequest xmlFlightRequest = objectFactory.createXmlFlightRequest();

            xmlFlightRequest.setAgentId(request.getId());
            xmlFlightRequest.setDepartureAirportCode(request.getDepartureAirportCode());
            xmlFlightRequest.setArrivalAirportCode(request.getArrivalAirportCode());
            xmlFlightRequest.setDepartureTime(request.getDepartureTime().getMillis());
            xmlFlightRequest.setNoOfPassengers(request.getNoOfPassengers());

            TextMessage message = session.createTextMessage();
            message.setText(JaxbUtil.generateXmlString(objectFactory.createXmlFlightRequest(xmlFlightRequest), XmlFlightRequest.class));
            message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_REQUEST);

            QueueSender sender = session.createSender(queue);
            sender.send(message);
        }
        catch (JMSException e) {
            throw new FailedIntegrationConnectionException(e.getMessage(), e);
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }
    }
}
