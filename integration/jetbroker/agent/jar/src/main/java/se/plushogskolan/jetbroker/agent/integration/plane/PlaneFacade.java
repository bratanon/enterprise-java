package se.plushogskolan.jetbroker.agent.integration.plane;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;

import javax.ejb.Local;
import java.util.List;

@Local
public interface PlaneFacade {

	public List<AirPort> getAllAirports();

	public List<PlaneType> getAllPlaneTypes();

}
