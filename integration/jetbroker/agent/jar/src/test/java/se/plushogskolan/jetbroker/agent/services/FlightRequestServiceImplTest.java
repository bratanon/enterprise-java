package se.plushogskolan.jetbroker.agent.services;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

    private FlightRequestServiceImp service;
    private FlightRequestRepository repo;
    private FlightRequest flightRequest;

    @Before
    public void setup() {
        service = new FlightRequestServiceImp();
        flightRequest = TestFixture.getValidFlightRequest();
        repo = EasyMock.createMock(FlightRequestRepository.class);
    }

    @Test
    public void testCreateFlightRequest() throws Exception {
        OrderFacade facade = EasyMock.createMock(OrderFacade.class);

        EasyMock.expect(repo.persist(flightRequest)).andReturn(1L);
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        EasyMock.replay(repo);
        service.setRepository(repo);

        facade.sendFlightRequest(flightRequest);
        EasyMock.replay(facade);
        service.setOrderFacade(facade);

        FlightRequest createFlightRequest = service.createFlightRequest(flightRequest);
        Assert.assertEquals("FlightRequest status is CREATED", FlightRequestStatus.CREATED, createFlightRequest.getRequestStatus());

        EasyMock.verify(repo, facade);
    }

    @Test
    public void testHandleFlightRequestConfirmation() {
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        repo.update(flightRequest);
        EasyMock.replay(repo);
        service.setRepository(repo);

        service.handleFlightRequestConfirmation(new FlightRequestConfirmation(1L, 1L));
        Assert.assertEquals("Status is REQUEST_CONFIRMED", FlightRequestStatus.REQUEST_CONFIRMED, flightRequest.getRequestStatus());
        Assert.assertEquals("ConfirmationId is set", 1L, flightRequest.getConfirmationId());
        EasyMock.verify(repo);
    }

    @Test
    public void testHandleFlightRequestRejection() {
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        repo.update(flightRequest);
        EasyMock.replay(repo);
        service.setRepository(repo);

        service.handleFlightRequestRejection(1);
        Assert.assertEquals("Status is REJECTED", FlightRequestStatus.REJECTED, flightRequest.getRequestStatus());
        Assert.assertNull("Offer is null", flightRequest.getOffer());

        EasyMock.verify(repo);
    }

    @Test
    public void testHandleFlightOffer() {
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        repo.update(flightRequest);
        EasyMock.replay(repo);
        service.setRepository(repo);

        FlightOffer flightOffer = new FlightOffer();

        service.handleFlightOffer(flightOffer, 1);
        Assert.assertEquals("Status is OFFER_RECEIVED", FlightRequestStatus.OFFER_RECEIVED, flightRequest.getRequestStatus());
        Assert.assertEquals("Offer is set", flightOffer, flightRequest.getOffer());

        EasyMock.verify(repo);
    }
}
