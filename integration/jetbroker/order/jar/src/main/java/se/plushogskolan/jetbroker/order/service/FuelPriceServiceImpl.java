package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class FuelPriceServiceImpl implements FuelPriceService {

    private static double fuelCost = 5.2;

    @Override
    public double getFuelCostPerLiter() {
        return fuelCost;
    }

    @Override
    public void updateFuelCostPerLiter(double fuelCost) {
        FuelPriceServiceImpl.fuelCost = fuelCost;
    }
}
