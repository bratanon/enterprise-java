package se.plushogskolan.jetbroker.order.repository.cache;

import java.util.List;

import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

public class CachedAirportRepository implements AirportRepository {

	@Inject
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public Airport getAirport(String code) {
		List<Airport> airports = planeIntegrationFacade.getAllAirports();

		for (Airport airport : airports) {
			if (airport.getCode().equals(code)) {
				return airport;
			}
		}
		return null;
	}

}
