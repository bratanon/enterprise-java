package se.plushogskolan.jetbroker.order.integration.agent;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

@Prod
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade {

    @Inject
    private Logger log;

    @Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
    private QueueConnectionFactory connectionFactory;

    @Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
    private Queue queue;

    @Override
    public void sendUpdatedOfferMessage(FlightRequest flightRequest) {
        log.fine("PROD: sendUpdatedOfferMessage: " + flightRequest);
        QueueConnection connection = null;
        QueueSession session = null;
        try {
            connection = connectionFactory.createQueueConnection();
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            connection.start();

            ObjectFactory objectFactory = new ObjectFactory();
            XmlFlightRequestOffer xmlOffer = objectFactory.createXmlFlightRequestOffer();
            xmlOffer.setAgentId(flightRequest.getAgentRequestId());
            xmlOffer.setOfferedPrice(flightRequest.getOffer().getPrice());
            xmlOffer.setOfferedPlaneTypeCode(flightRequest.getOffer().getPlane().getPlaneTypeCode());

            TextMessage message = session.createTextMessage();
            message.setText(JaxbUtil.generateXmlString(objectFactory.createXmlFlightRequestOffer(xmlOffer), XmlFlightRequestOffer.class));
            message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_OFFER);

            QueueSender sender = session.createSender(queue);
            sender.send(message);
        }
        catch (Exception e) {
            throw new FailedIntegrationConnectionException(e.getMessage());
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }
    }

    @Override
    public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {
        log.fine("PROD: sendFlightRequestRejectedMessage: " + flightRequest);
        QueueConnection connection = null;
        QueueSession session = null;
        try {
            connection = connectionFactory.createQueueConnection();
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            connection.start();

            MapMessage message = session.createMapMessage();
            message.setLong("agentId", flightRequest.getAgentRequestId());
            message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_REJECTION);

            QueueSender sender = session.createSender(queue);
            sender.send(message);
        }
        catch (JMSException e) {
            throw new RuntimeException(e);
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }

    }

    @Override
    public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {
        log.fine("PROD: sendFlightRequestConfirmation: " + response);

        QueueConnection connection = null;
        QueueSession session = null;
        try {
            connection = connectionFactory.createQueueConnection();
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            connection.start();

            MapMessage message = session.createMapMessage();
            message.setLong("agentId", response.getAgentRequestId());
            message.setLong("confirmationId", response.getOrderRequestId());
            message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION);

            QueueSender sender = session.createSender(queue);
            sender.send(message);
        }
        catch (JMSException e) {
            throw new RuntimeException(e);
        }
        finally {
            JmsHelper.closeConnectionAndSession(connection, session);
        }
    }

}
