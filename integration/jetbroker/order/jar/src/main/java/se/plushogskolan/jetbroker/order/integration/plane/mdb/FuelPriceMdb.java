package se.plushogskolan.jetbroker.order.integration.plane.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Topic"
        ),
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST
        ),
        @ActivationConfigProperty(
                propertyName = "messageSelector",
                propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED + "'"
        )
})
public class FuelPriceMdb extends AbstractMDB implements MessageListener {

    @Inject
    Logger log;

    @Inject
    private FuelPriceService fuelPriceService;

    @Override
    public void onMessage(Message message) {
        log.fine("Incoming FuelPriceChanged message");

        try {
            fuelPriceService.updateFuelCostPerLiter(message.getDoubleProperty("fuelPrice"));
        }
        catch (JMSException e) {
            throw new FailedIntegrationConnectionException(e.getMessage(), e);
        }

    }
}
