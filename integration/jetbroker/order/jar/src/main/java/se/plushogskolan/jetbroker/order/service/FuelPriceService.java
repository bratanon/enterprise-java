package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public interface FuelPriceService {

    @Lock(LockType.READ)
    public double getFuelCostPerLiter();

    @Lock(LockType.WRITE)
    public void updateFuelCostPerLiter(double fuelCost);

}
