package se.plushogskolan.jetbroker.order.integration.agent.mbd;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(
                propertyName = "destinationType",
                propertyValue = "javax.jms.Queue"
        ),
        @ActivationConfigProperty(
                propertyName = "destination",
                propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST
        ),
        @ActivationConfigProperty(
                propertyName = "messageSelector",
                propertyValue = "messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_REQUEST + "'"
        )
})
public class FlightReqeustMdb extends AbstractMDB implements MessageListener {

    @Inject
    private Logger log;

    @Inject
    private FlightRequestService flightRequestService;

    @Override
    public void onMessage(Message message) {
        log.fine("Incoming FlightReqeust message.");
        try {
            TextMessage msg = (TextMessage) message;

            XmlFlightRequest xmlFlightRequest = (XmlFlightRequest) JaxbUtil.generateJaxbObject(msg.getText(), XmlFlightRequest.class);

            FlightRequest flightRequest = new FlightRequest();

            flightRequest.setAgentRequestId(xmlFlightRequest.getAgentId());
            flightRequest.setDepartureAirportCode(xmlFlightRequest.getDepartureAirportCode());
            flightRequest.setArrivalAirportCode(xmlFlightRequest.getArrivalAirportCode());
            flightRequest.setNoOfPassengers(xmlFlightRequest.getNoOfPassengers());
            flightRequest.setDepartureTime(new DateTime(xmlFlightRequest.getDepartureTime()));

            flightRequestService.handleIncomingFlightRequest(flightRequest);
        }
        catch (Exception e) {
            throw new FailedIntegrationConnectionException(e.getMessage());
        }

    }
}
