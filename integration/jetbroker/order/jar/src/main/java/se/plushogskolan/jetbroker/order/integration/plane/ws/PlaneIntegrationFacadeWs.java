package se.plushogskolan.jetbroker.order.integration.plane.ws;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
public class PlaneIntegrationFacadeWs implements PlaneIntegrationFacade {

    @Inject
    Logger log;

    @Override
    public List<Airport> getAllAirports () {
        log.fine("Get all airports");

        final PlaneWebServiceImplService service = new PlaneWebServiceImplService();
        final PlaneWebService port = service.getPlaneWebServicePort();

        final List<WsAirport> airportsFromWs = port.getAirports();

        final List<Airport> airports = new ArrayList<Airport>();
        for (WsAirport wsAirport : airportsFromWs) {
            airports.add(convertWsAirportToAirport(wsAirport));
        }

        return airports;
    }

    @Override
    public List<PlaneType> getAllPlaneTypes () {
        log.fine("In get all plane types");

        final PlaneWebServiceImplService service = new PlaneWebServiceImplService();
        final PlaneWebService port = service.getPlaneWebServicePort();

        final List<WsPlaneType> planeTypesFromWs = port.getPlaneTypes();

        final List<PlaneType> planeTypes = new ArrayList<PlaneType>();
        for (WsPlaneType wsPlaneType : planeTypesFromWs) {
            planeTypes.add(convertWsPlaneTypeToPlaneType(wsPlaneType));
        }

        return planeTypes;
    }

    private Airport convertWsAirportToAirport(WsAirport wsAirport) {
        Airport airport = new Airport();
        airport.setCode(wsAirport.getCode());
        airport.setName(wsAirport.getName());
        airport.setLatitude(wsAirport.getLatitude());
        airport.setLongitude(wsAirport.getLongitude());

        return airport;
    }

    private PlaneType convertWsPlaneTypeToPlaneType(WsPlaneType wsPlaneType) {
        PlaneType planeType = new PlaneType();
        planeType.setCode(wsPlaneType.getCode());
        planeType.setName(wsPlaneType.getName());
        planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
        planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
        planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
        planeType.setFuelConsumptionPerKm(wsPlaneType.getFuelConsumptionPerKm());

        return planeType;
    }
}
