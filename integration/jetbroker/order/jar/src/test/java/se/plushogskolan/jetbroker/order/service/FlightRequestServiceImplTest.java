package se.plushogskolan.jetbroker.order.service;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

    private FlightRequestServiceImpl service = new FlightRequestServiceImpl();

    private PlaneService planeService;
    private FlightRequestRepository repo;
    private AgentIntegrationFacade facade;
    private FlightRequest flightRequest;

    @Before
    public void setup() {
        planeService = EasyMock.createMock(PlaneService.class);
        repo = EasyMock.createMock(FlightRequestRepository.class);
        facade = EasyMock.createMock(AgentIntegrationFacade.class);
        flightRequest = TestFixture.getValidFlightRequest();

        service.setPlaneService(planeService);
        service.setFlightRequestRepository(repo);
        service.setAgentIntegrationFacade(facade);
    }

    @Test
    public void testHandleIncomingFlightRequest() {
        Airport airport1 = new Airport("GBG", "Gothenburg [Mocked]", 57.697, 11.98);
        Airport airport2 = new Airport("STM", "Stockholm [Mocked]", 59.33, 18.06);
        EasyMock.expect(planeService.getAirport(flightRequest.getArrivalAirportCode())).andReturn(airport1);
        EasyMock.expect(planeService.getAirport(flightRequest.getDepartureAirportCode())).andReturn(airport2);
        EasyMock.replay(planeService);

        EasyMock.expect(repo.persist(flightRequest)).andReturn(1L);
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        EasyMock.replay(repo);

        facade.sendFlightRequestConfirmation(EasyMock.isA(FlightRequestConfirmation.class));
        EasyMock.replay(facade);

        FlightRequestConfirmation confirmation = service.handleIncomingFlightRequest(flightRequest);
        Assert.assertEquals("Confirmation agentRequestId", 10, confirmation.getAgentRequestId());
        Assert.assertEquals("Confirmation orderRequestId", 1, confirmation.getOrderRequestId());
        Assert.assertEquals("Correct distance", 396, flightRequest.getDistanceKm());
        Assert.assertEquals("Correct FlightRequestStatus", FlightRequestStatus.NEW, flightRequest.getStatus());

        EasyMock.verify(planeService, repo, facade);
    }

    @Test
    public void testRejectFlightRequest() {
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        repo.update(flightRequest);
        EasyMock.replay(repo);

        facade.sendFlightRequestRejectedMessage(flightRequest);
        EasyMock.replay(facade);

        service.rejectFlightRequest(1);

        Assert.assertEquals("Status is REJECTED_BY_US", FlightRequestStatus.REJECTED_BY_US, flightRequest.getStatus());
        Assert.assertNull("Offer is null", flightRequest.getOffer());

        EasyMock.verify(repo, facade);
    }

    @Test
    public void testHandleUpdatedOffer() {
        EasyMock.expect(repo.findById(1L)).andReturn(flightRequest);
        repo.update(flightRequest);
        EasyMock.replay(repo);

        facade.sendUpdatedOfferMessage(flightRequest);
        EasyMock.replay(facade);
        Offer offer = new Offer();
        service.handleUpdatedOffer(1, offer);

        Assert.assertEquals("Status is OFFER_SENT", FlightRequestStatus.OFFER_SENT, flightRequest.getStatus());
        Assert.assertEquals("Offer is set", offer, flightRequest.getOffer());

        EasyMock.verify(repo, facade);
    }
}
