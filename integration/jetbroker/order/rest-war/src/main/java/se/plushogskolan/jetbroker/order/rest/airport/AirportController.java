package se.plushogskolan.jetbroker.order.rest.airport;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.rest.airport.model.GetFuelPriceResponse;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;

@Controller
public class AirportController {

    Logger log = Logger.getLogger(AirportController.class.getName());

    @Inject
    private FuelPriceService fuelPriceService;

    @RequestMapping(value = "/getFuelPrice", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetFuelPriceResponse getPlaneType() {
        log.severe("CLASSIC BERTIL - DET ÄR ARKITEKTENS FEL");
        double fuelCost = fuelPriceService.getFuelCostPerLiter();

        return new GetFuelPriceResponse(fuelCost);
    }

}
