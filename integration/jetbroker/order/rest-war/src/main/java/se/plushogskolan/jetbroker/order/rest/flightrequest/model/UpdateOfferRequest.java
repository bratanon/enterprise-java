package se.plushogskolan.jetbroker.order.rest.flightrequest.model;

public class UpdateOfferRequest {

	private double price;
	private long planeId;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

	@Override
	public String toString() {
		return "UpdateOfferRequest [price=" + price + ", planeId=" + planeId + "]";
	}

}
