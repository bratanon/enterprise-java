package se.plushogskolan.jetbroker.order.rest.plane;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneRequest;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@RestController
public class PlaneController {

    @Inject
    private PlaneService planeService;

    @RequestMapping(value = "createPlane", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Plane> createPlane(@RequestBody CreatePlaneRequest request) {
        Plane plane = null;
        HttpStatus statusCode = HttpStatus.CREATED;
        try {
            plane = planeService.createPlane(request.buildPlane());
        }
        catch (Exception e) {
            statusCode = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<Plane>(plane, statusCode);
    }

    @RequestMapping(value = "getPlane/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Plane> getPlane(@PathVariable long id) {
        Plane plane = planeService.getPlane(id);
        HttpStatus statusCode = HttpStatus.OK;
        if (plane == null) {
            statusCode = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<Plane>(plane, statusCode);
    }

    @RequestMapping(value = "deletePlane/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<Plane> deletePlane(@PathVariable long id) {
        Plane plane = planeService.getPlane(id);
        HttpStatus statusCode = HttpStatus.OK;

        if (plane == null) {
            statusCode = HttpStatus.NOT_FOUND;
        }

        try {
            planeService.deletePlane(plane.getId());
        }
        catch (Exception e) {
            plane = null;
            statusCode = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<Plane>(plane, statusCode);
    }
}
