package se.plushogskolan.jee.utils.validation;

/**
 * Interface implemented by classes that hold both an arrival airport and
 * departure airport. Validators can use this interface for their validation.
 * 
 */
public interface ArrivalAndDepartureAirportHolder {

	public String getDepartureAirportCode();

	public String getArrivalAirportCode();
}
