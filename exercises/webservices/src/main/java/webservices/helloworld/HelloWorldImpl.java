package webservices.helloworld;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Implement the HelloWorld web service
 */
@WebService(endpointInterface = "webservices.helloworld.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	public static void main(String[] args) {
		// Implement so that this web service can start itself
	}

    @WebMethod
	public String helloWorld() {
		return "Hello world";
	}

}
