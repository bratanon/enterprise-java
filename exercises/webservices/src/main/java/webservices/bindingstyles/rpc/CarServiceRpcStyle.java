package webservices.bindingstyles.rpc;

import webservices.bindingstyles.CarService;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Add annotations to make this an RPC style web service
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CarServiceRpcStyle extends CarService {

}
