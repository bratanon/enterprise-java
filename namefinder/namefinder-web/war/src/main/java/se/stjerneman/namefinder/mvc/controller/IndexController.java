package se.stjerneman.namefinder.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import se.stjerneman.namefinder.model.NameFrinderResults;
import se.stjerneman.namefinder.mvc.bean.SearchBean;
import se.stjerneman.namefinder.rest.RestService;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
@Controller
public class IndexController {

    private Logger log = Logger.getLogger(IndexController.class.getName());

    @Inject
    private RestService restService;

    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView indexPage (@RequestParam(value = "mock", required = false) boolean mock) {

        log.info("Index controller : " + mock);

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("searchBean", new SearchBean());
        return mav;
    }

    @RequestMapping(value = "/index.html", method = RequestMethod.POST)
    public ModelAndView searchSubmit (SearchBean bean, @RequestParam(value = "mock", required = false) boolean mock) {
        ModelAndView mav = new ModelAndView("index");

        final List<NameFrinderResults> results = restService.getResults(bean.getSearchString(), mock);

        SearchBean searchBean = new SearchBean();
        searchBean.setSearchString(bean.getSearchString());

        mav.addObject("searchBean", searchBean);
        mav.addObject("searchResults", results);

        return mav;
    }

    @RequestMapping(value = "/{searchString}.ajax", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView ajaxSearch (@PathVariable String searchString, @RequestParam(value = "mock", required = false) boolean mock) {
        ModelAndView mav = new ModelAndView("resultList");

        final List<NameFrinderResults> results = restService.getResults(searchString, mock);
        mav.addObject("searchResults", results);
        return mav;
    }
}
