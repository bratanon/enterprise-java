package se.stjerneman.namefinder.mvc.bean;

/**
 * @author Emil Stjerneman
 */
public class SearchBean {

    private String searchString;

    public String getSearchString () {
        return searchString;
    }

    public void setSearchString (String searchString) {
        this.searchString = searchString;
    }
}
