package se.stjerneman.namefinder.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import se.stjerneman.namefinder.model.NameFrinderResults;
import se.stjerneman.namefinder.mvc.bean.SearchBean;
import se.stjerneman.namefinder.rest.RestService;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Emil Stjerneman
 */
@Controller
public class MiscController {

    @RequestMapping(value = "/api.html", method = RequestMethod.GET)
    public ModelAndView apiPage() {
        ModelAndView mav = new ModelAndView("api");
        return mav;
    }
}
