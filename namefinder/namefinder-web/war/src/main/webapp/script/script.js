/**
 * Created by Emil Stjerneman on 2014-10-22.
 */

(function ($) {

    $(function () {
        $('form').submit(function (event) {
            var searchString = $(this).find('input').val();

            if (searchString.length == 0) {
                event.preventDefault();
                return false;
            }

            var throbber = $('<div></div>').addClass('throbber').html('<i class="fa fa-spinner fa-spin"></i>');

            $('.resultHolder').empty();
            $('.resultHolder').html(throbber);

            var url = "/namefinder-web/" + searchString + ".ajax";

            if (GetURLParameter('mock')) {
                url += "?mock=true";
            }

            var request = $.get(url, function (data) {
                $('.resultHolder').empty().hide();
                $('.resultHolder').html(data).fadeIn();
            }).fail(function () {
                var error = $('<div></div>').addClass('error').html('<i class="fa fa-exclamation-triangle"></i> Reqiest error');
                $('.resultHolder').empty().hide();
                $('.resultHolder').html(error).fadeIn();
            });

            event.preventDefault();
        });
    });

    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

})(jQuery);
