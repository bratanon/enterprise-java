<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty searchResults}">
    <h2><c:out value="Results for ${searchBean.searchString}"/></h2>
    <ul class="results">
        <c:forEach items="${searchResults}" var="result">
            <li>
                <div class="result">
                    <h3><c:out value="${result.title}"/></h3>
                    <c:choose>
                        <c:when test="${result.isAvailability()}">
                            <i class="fa fa-check"></i>
                        </c:when>
                        <c:when test="${not result.isAvailability()}">
                            <i class="fa fa-times"></i>
                        </c:when>
                    </c:choose>
                </div>
            </li>
        </c:forEach>
        <li>
            <ul class="result-desc">
                <li><i class="fa fa-times"></i> Taken</li>
                <li><i class="fa fa-check"></i> Available</li>
            </ul>
        </li>
    </ul>

</c:if>

