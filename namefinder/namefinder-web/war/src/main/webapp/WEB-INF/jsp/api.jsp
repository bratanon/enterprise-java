<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>API - NameFinder</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/style/reset.css"
          media="all"/>
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,900,400italic,700italic,900italic"
          media="all"/>
    <link rel='stylesheet'
          href='//fonts.googleapis.com/css?family=Lekton:400,700,400italic|Fredoka+One|Julius+Sans+One'
          media="all">
    <link rel='stylesheet'
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
          media="all">
    <!--link rel="stylesheet" href="http://namefinder.static.dev/style/style.css" /-->
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/style/style.css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="<%=request.getContextPath()%>/script/script.js"></script>
</head>
<body class="api">
<div class="wrapper">
    <h1>NameFinder API</h1>

    <h2>Search</h2>

    <div class="content">
        <p>Will fetch the availability status from all resource providers.</p>

        <h3>URL</h3>
        <code>http://localhost:8080/namefinder-api/search/:searchString</code>

        <h3>Method</h3>
        <code>GET</code>

        <h3>URL Params</h3>
        <code>searchString=[string] <strong>(required)</strong></code>

        <h3>Example response</h3>
        <code>
<pre>[
    {
        "name": "Domainr",
        "title": "example.se",
        "availability": false
    },
    {
        "name": "Domainr",
        "title": "example.eu",
        "availability": true
    },
    {
        "name": "Domainr",
        "title": "example.com",
        "availability": true
    },
    {
        "name": "Domainr",
        "title": "example.dk",
        "availability": true
    },
    {
        "name": "Domainr",
        "title": "example.info",
        "availability": false
    },
    {
        "name": "Domainr",
        "title": "example.org",
        "availability": false
    },
    {
        "name": "Domainr",
        "title": "example.net",
        "availability": false
    },
    {
        "name": "Twitter",
        "title": "Twitter",
        "availability": true
    }
]</pre>
        </code>
    </div>
</div>

<footer>
    <nav>
        <ul>
            <li><a href="<%=request.getContextPath()%>/">Start</a></li>
            <li><a href="<%=request.getContextPath()%>/api.html">API</a></li>
        </ul>
    </nav>
</footer>

</body>
</html>
