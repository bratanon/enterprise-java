<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NameFinder</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/style/reset.css"
          media="all"/>
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,900,400italic,700italic,900italic"
          media="all"/>
    <link rel='stylesheet'
          href='//fonts.googleapis.com/css?family=Lekton:400,700,400italic|Fredoka+One|Julius+Sans+One'
          media="all"/>
    <link rel='stylesheet'
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
          media="all"/>
    <!--link rel="stylesheet" href="http://namefinder.static.dev/style/style.css" /-->
    <link rel="stylesheet"
          href="<%=request.getContextPath()%>/style/style.css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    <script src="<%=request.getContextPath()%>/script/script.js"></script>
</head>
<body class="front">
<div class="wrapper">
    <h1>NameFinder</h1>

    <form:form commandName="searchBean">
        <form:input path="searchString" autocomplete="off"/>
    </form:form>

    <h2> - your guide to the perfect name</h2>

    <div class="resultHolder">
        <jsp:include page="resultList.jsp"/>
    </div>
</div>

<footer>
    <nav>
        <ul>
            <li><a href="<%=request.getContextPath()%>/">Start</a></li>
            <li><a href="<%=request.getContextPath()%>/api.html">API</a></li>
        </ul>
    </nav>
</footer>

</body>
</html>
