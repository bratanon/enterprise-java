package se.stjerneman.namefinder.model;

/**
 * @author Emil Stjerneman
 */
public class NameFrinderResults implements Comparable<NameFrinderResults> {

    private String name;

    private String title;

    private boolean availability;

    public NameFrinderResults() {

    }

    public NameFrinderResults(String name, String title, boolean availability) {
        setName(name);
        setTitle(title);
        setAvailability(availability);
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public boolean isAvailability () {
        return availability;
    }

    public void setAvailability (boolean availability) {
        this.availability = availability;
    }

    @Override
    public int compareTo (NameFrinderResults o) {
        if (getName().compareToIgnoreCase(o.getName()) != 0) {
            return getName().compareToIgnoreCase(o.getName());
        }
        else {
            return getTitle().compareToIgnoreCase(o.getTitle());
        }
    }
}
