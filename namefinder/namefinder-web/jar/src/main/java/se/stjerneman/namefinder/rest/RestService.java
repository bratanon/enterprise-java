package se.stjerneman.namefinder.rest;

import se.stjerneman.namefinder.model.NameFrinderResults;

import java.util.List;

/**
 * @author Emil Stjerneman
 */
public interface RestService {

    public List<NameFrinderResults> getResults (String searchString, boolean useMock);
}
