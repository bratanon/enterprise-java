package se.stjerneman.namefinder.rest;

import org.springframework.web.client.RestTemplate;
import se.stjerneman.namefinder.model.NameFrinderResults;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Emil Stjerneman
 */
@Stateless
public class DefaultRestService implements RestService {

    private Logger log = Logger.getLogger(DefaultRestService.class.getName());

    private final static String BASE_URI = "http://localhost:8080/namefinder-api/search/";

    public List<NameFrinderResults> getResults (String searchString, boolean useMock) {
        final RestTemplate restTemplate = new RestTemplate();
        NameFrinderResults[] results;

        if (!useMock) {
            results = restTemplate.getForObject(BASE_URI + searchString, NameFrinderResults[].class);
        }
        else {
            // DEMO DATA
            results = new NameFrinderResults[6];
            results[0] = new NameFrinderResults("domain", "example.dk", true);
            results[1] = new NameFrinderResults("domain", "example.info", false);
            results[2] = new NameFrinderResults("twitter", "Twitter", true);
            results[3] = new NameFrinderResults("domain", "example.se", true);
            results[4] = new NameFrinderResults("domain", "example.com", false);
            results[5] = new NameFrinderResults("domain", "example.net", true);
        }

        log.fine("RESULTS IN LIST " + results.length);

        List<NameFrinderResults> sortedList = new ArrayList<>(Arrays.asList(results));
        Collections.sort(sortedList);

        return sortedList;

    }
}
