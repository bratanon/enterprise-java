package se.stjerneman.namefinder;

import se.stjerneman.namefinder.resourceprovider.domainr.DomainrModel;

/**
 * @author Emil Stjerneman
 */
public class TestFixture {

    public static DomainrModel getDomainrModel (String domain, boolean availability) {
        final DomainrModel domainrModel = new DomainrModel();
        domainrModel.setDomain(domain);
        domainrModel.setAvailability(availability ? "available" : "taken");
        return domainrModel;
    }
}
