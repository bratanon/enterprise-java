package se.stjerneman.namefinder.domain;

import junit.framework.Assert;
import org.junit.Test;

public class ResourceItemTest {

    @Test
    public void ResourceItemTest () {
        ResourceItem resourceItem = new ResourceItem("Foo", "Bar", true);
        Assert.assertEquals(resourceItem.getName(), "Foo");
        Assert.assertEquals(resourceItem.getTitle(), "Bar");
        Assert.assertTrue(resourceItem.isAvailability());
    }
}
