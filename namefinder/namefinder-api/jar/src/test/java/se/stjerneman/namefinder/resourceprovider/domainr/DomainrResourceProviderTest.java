package se.stjerneman.namefinder.resourceprovider.domainr;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import se.stjerneman.namefinder.TestFixture;
import se.stjerneman.namefinder.domain.ResourceItem;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.easymock.EasyMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;


public class DomainrResourceProviderTest {

    private final DomainrResourceProvider provider = new DomainrResourceProvider();

    private RestTemplate restTemplate;

    @Before
    public void setup () {
        restTemplate = createMock(RestTemplate.class);
        provider.setRestTemplate(restTemplate);
    }

    @Test
    public void testGetResults () {
        // Create return objects.
        final DomainrModel testCom = TestFixture.getDomainrModel("test.com", false);
        final DomainrModel testSe = TestFixture.getDomainrModel("test.se", true);

        // Sets a shot tld list.
        provider.setTlds(new HashSet<>(Arrays.asList(".com", ".se")));

        Capture<Class<DomainrModel>> classCapture = new Capture<>();
        expect(restTemplate.getForObject(isA(URI.class), capture(classCapture))).andReturn(testCom);
        expect(restTemplate.getForObject(isA(URI.class), capture(classCapture))).andReturn(testSe);
        replay(restTemplate);

        // Call service.
        final Set<ResourceItem> results = provider.getResults("test");

        verify(restTemplate);

        assertEquals("List size is correct. ", 2, results.size());
        assertThat(results, containsInAnyOrder(new ResourceItem("Domainr", "test.com", false), new ResourceItem("Domainr", "test.se", true)));
    }
}
