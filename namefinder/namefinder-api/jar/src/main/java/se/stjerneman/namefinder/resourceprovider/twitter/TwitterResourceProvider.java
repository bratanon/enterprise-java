package se.stjerneman.namefinder.resourceprovider.twitter;

import org.springframework.social.ApiException;
import org.springframework.social.ServerOverloadedException;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import se.stjerneman.namefinder.domain.ResourceItem;
import se.stjerneman.namefinder.resourceprovider.ResourceProvider;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * A resource provider that fetches data from the Twitter API.
 *
 * NOTE: It uses a user based authentication and not an application-only
 * authentication to higher the request limit.
 *
 * @author Emil Stjerneman
 */
public class TwitterResourceProvider implements ResourceProvider {

    private Logger log = Logger.getLogger(TwitterResourceProvider.class.getName());

    private Twitter twitter;

    private Properties properties;

    @Override
    public Set<ResourceItem> getResults (String searchString) {
        log.fine("In TwitterResourceProvider");

        try {
            readPropertiesFromFile();
        }
        catch (IOException e) {
            log.severe("Could not load the twitter property file. " + e.getMessage());
            return new HashSet<>();
        }

        setTwitter(new TwitterTemplate(properties.getProperty("consumerKey"), properties.getProperty("consumerSecret"), properties.getProperty("accessToken"), properties.getProperty("accessTokenSecret")));

        List<TwitterProfile> profiles = null;
        try {
            profiles = getTwitter().userOperations().searchForUsers(searchString);
        }
        catch (ServerOverloadedException e) {
            log.info("Could not fetch data from twitter API. " + e.getMessage());
            return new HashSet<>();
        }
        catch (ApiException e) {
            log.severe("Could not fetch data from twitter API. " + e.getMessage());
            return new HashSet<>();
        }

        log.fine("Got " + profiles.size() + " profiles from twitter search.");

        ResourceItem resourceItem = new ResourceItem("Twitter", "Twitter", true);

        // The twitter search does not support exact searches.
        // Loop through the list and try to find an exact match.
        for (TwitterProfile p : profiles) {
            if (p.getScreenName().equals(searchString)) {
                log.fine("Found exact match in twitter profile list.");
                resourceItem.setAvailability(false);
            }
        }

        return new HashSet<>(Arrays.asList(resourceItem));
    }

    /**
     * Reads properties from the property file.
     *
     * @throws java.io.IOException
     */
    private void readPropertiesFromFile () throws IOException {
        properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("twitter.properties"));
    }

    public Twitter getTwitter () {
        return twitter;
    }

    public void setTwitter (Twitter twitter) {
        this.twitter = twitter;
    }
}
