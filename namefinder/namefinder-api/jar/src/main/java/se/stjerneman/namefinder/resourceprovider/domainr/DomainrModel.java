package se.stjerneman.namefinder.resourceprovider.domainr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * This is a model pojo used by the Domainr resource provider them fetching data
 * from their REST service.
 *
 * @author Emil Stjerneman
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomainrModel {

    private String domain;

    private String availability;

    public String getDomain () {
        return domain;
    }

    public void setDomain (String domain) {
        this.domain = domain;
    }

    public String getAvailability () {
        return availability;
    }

    public void setAvailability (String availability) {
        this.availability = availability;
    }

    @Override
    public String toString () {
        return "Domainr{" +
                "domain='" + domain + '\'' +
                ", availability='" + availability + '\'' +
                '}';
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DomainrModel that = (DomainrModel) o;

        if (availability != null ? !availability.equals(that.availability) : that.availability != null) {
            return false;
        }
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode () {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (availability != null ? availability.hashCode() : 0);
        return result;
    }
}
