package se.stjerneman.namefinder.resourceprovider;

import se.stjerneman.namefinder.domain.ResourceItem;

import java.util.Set;

/**
 * A resource provider is responsible for handling REST requests and return the
 * results from them.
 *
 * Its important that all resource providers implement this interface as they
 * will be autoloaded by a {@link se.stjerneman.namefinder.service.ResourceProviderService}.
 *
 * @author Emil Stjerneman
 */
public interface ResourceProvider {

    /**
     * Gets results.
     *
     * NOTE: Do NOT return null, return an empty Set<ResourceItem> instead.
     *
     * @param searchString
     *         the search word.
     *
     * @return a set of results.
     */
    public Set<ResourceItem> getResults (String searchString);

}
