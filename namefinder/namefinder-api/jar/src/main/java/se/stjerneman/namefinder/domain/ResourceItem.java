package se.stjerneman.namefinder.domain;

/**
 * A POJO used for generic resource provider results.
 */
public class ResourceItem {

    /**
     * This resource items name.
     */
    private String name;

    /**
     * This resource items title.
     */
    private String title;

    /**
     * This resource items availability status.
     * // TODO: Should this be an ENUM instead
     */
    private boolean availability;

    /**
     * Create a new ResourceItem.
     *
     * @param name
     *         this resources name
     * @param title
     *         this resources title. This is used for presentation.
     * @param availability
     *         this resources availability status.
     */
    public ResourceItem (String name, String title, boolean availability) {
        setName(name);
        setTitle(title);
        setAvailability(availability);
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public boolean isAvailability () {
        return availability;
    }

    public void setAvailability (boolean availability) {
        this.availability = availability;
    }

    @Override
    public String toString () {
        return "ResourceItem{" +
                "name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", availability=" + availability +
                '}';
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ResourceItem that = (ResourceItem) o;

        if (availability != that.availability) {
            return false;
        }
        if (!name.equals(that.name)) {
            return false;
        }
        if (!title.equals(that.title)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode () {
        int result = name.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + (availability ? 1 : 0);
        return result;
    }
}
