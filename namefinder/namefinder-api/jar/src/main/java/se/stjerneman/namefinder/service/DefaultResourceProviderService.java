package se.stjerneman.namefinder.service;

import org.reflections.Reflections;
import se.stjerneman.namefinder.domain.ResourceItem;
import se.stjerneman.namefinder.resourceprovider.ResourceProvider;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * The default resource provider service.
 *
 * @author Emil Stjerneman
 */
@Stateless
public class DefaultResourceProviderService implements ResourceProviderService {

    @Inject
    private Logger log;

    @Override
    public Set<Class<? extends ResourceProvider>> getResourceProviders () {
        final Reflections reflections = new Reflections("se.stjerneman.namefinder.resourceprovider");
        return reflections.getSubTypesOf(ResourceProvider.class);
    }

    @Override
    public Set<ResourceItem> getResults (String searchString) {
        log.fine("About to get results.");

        Set<ResourceItem> results = new HashSet<>();

        for (Class<? extends ResourceProvider> resourceProvider : getResourceProviders()) {
            try {
                final Class<?> clazz = Class.forName(resourceProvider.getName());

                // Do not instance abstract classes.
                if (Modifier.isAbstract(clazz.getModifiers())) {
                    continue;
                }

                // Instace the class.
                final ResourceProvider provider = (ResourceProvider) clazz.newInstance();

                results.addAll(provider.getResults(searchString));
            }
            catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                // TODO: Handle this.
                log.severe("Something went terribly wrong when trying to get results.");
                e.printStackTrace();
            }
        }

        return results;
    }
}
