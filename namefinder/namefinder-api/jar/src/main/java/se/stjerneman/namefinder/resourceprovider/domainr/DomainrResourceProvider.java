package se.stjerneman.namefinder.resourceprovider.domainr;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import se.stjerneman.namefinder.domain.ResourceItem;
import se.stjerneman.namefinder.resourceprovider.ResourceProvider;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A resource provider that fetches data from the Domainr API.
 *
 * @author Emil Stjerneman
 * @see se.stjerneman.namefinder.resourceprovider.domainr.DomainrModel
 */
public class DomainrResourceProvider implements ResourceProvider {

    private Logger log = Logger.getLogger(DomainrResourceProvider.class.getName());

    // Base URL
    private static final String BASE_URI = "https://domainr.com/api/json/info";

    // Set of TLDs to search for.
    private Set<String> tlds = new HashSet<>(Arrays.asList(".com", ".se", ".net", ".info", ".dk", ".org", ".eu"));

    private Properties properties;

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public Set<ResourceItem> getResults (String searchString) {
        log.fine("In DomainrResourceProvider");
        Set<ResourceItem> results = new HashSet<>();

        try {
            readPropertiesFromFile();
        }
        catch (IOException e) {
            log.severe("Could not load the domainr property file. " + e.getMessage());
            return results;
        }

        for (String tld : getTlds()) {
            // TODO: Handle exceptions here.. 400, 500 and so on.
            DomainrModel response = getRestTemplate().getForObject(buildUri(searchString, tld), DomainrModel.class);

            boolean available = !response.getAvailability().equals("taken");
            results.add(new ResourceItem("Domainr", searchString + tld, available));
        }

        return results;
    }

    /**
     * Build the callable API uri.
     *
     * @param searchString
     *         the search word.
     * @param tld
     *         the tld (top level domain) to prepend on the search word.
     *
     * @return a FQDN.
     */
    private URI buildUri (String searchString, String tld) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(DomainrResourceProvider.BASE_URI)
                .queryParam("client_id", properties.getProperty("clintId"))
                .queryParam("q", searchString + tld);

        final URI uri = builder.build().toUri();
        log.fine("Calling URI: " + uri.toString());

        return uri;
    }

    /**
     * Reads properties from the property file.
     *
     * @throws IOException
     */
    private void readPropertiesFromFile () throws IOException {
        properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("domainr.properties"));
    }

    public RestTemplate getRestTemplate () {
        return restTemplate;
    }

    public void setRestTemplate (RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Set<String> getTlds () {
        return tlds;
    }

    public void setTlds (Set<String> tlds) {
        this.tlds = tlds;
    }
}
