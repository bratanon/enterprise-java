package se.stjerneman.namefinder.service;

import se.stjerneman.namefinder.domain.ResourceItem;
import se.stjerneman.namefinder.resourceprovider.ResourceProvider;

import java.util.Set;

/**
 * @author Emil Stjerneman
 */
public interface ResourceProviderService {

    /**
     * Gets a set of resource providers that will be queried for results.
     *
     * @return a set with resource provider classes.
     */
    public Set<Class<? extends ResourceProvider>> getResourceProviders ();

    /**
     * Gets a set of results from the resource provider calls.
     *
     * @param searchString
     *         the search word.
     *
     * @return a set of results from the resource provider calls.
     */
    public Set<ResourceItem> getResults (String searchString);
}
