package se.stjerneman.namefinder.mvc;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import se.stjerneman.namefinder.domain.ResourceItem;
import se.stjerneman.namefinder.service.ResourceProviderService;

import javax.inject.Inject;
import java.util.Set;

/**
 *
 */
@RestController
public class IndexController {

    @Inject
    private ResourceProviderService resourceProviderService;

	@RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<ResourceItem>> search(@PathVariable String name) {
        final Set<ResourceItem> results = resourceProviderService.getResults(name);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
