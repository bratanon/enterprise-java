package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class LongitudeValidatorTest {

    private LongitudeValidator validator;

    @Before
    public void setup() {
        validator = new LongitudeValidator();
    }

    @Test
    public void testIsValid_okString() {
        assertTrue("Ok - String", validator.isValid("10", null));
    }

    @Test
    public void testIsValid_okInteger() {
        assertTrue("Ok - Integer", validator.isValid(10, null));
    }

    @Test
    public void testIsValid_okDouble() {
        assertTrue("Ok - Double", validator.isValid(10.0, null));
    }

    @Test
    public void testIsValid_zero() {
        assertFalse("Zero", validator.isValid(0, null));
    }

    @Test
    public void testIsValid_null() {
        assertFalse("Null", validator.isValid(null, null));
    }

    @Test
    public void testIsValid_empty() {
        assertFalse("Empty", validator.isValid(null, null));
    }

    @Test
    public void testIsValid_tooLow() {
        assertFalse("Low", validator.isValid(-181, null));
    }

    @Test
    public void testIsValid_tooHigh() {
        assertFalse("High", validator.isValid(181, null));
    }
}
