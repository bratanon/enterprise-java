package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ArrivalAndDepartureAirportValidatorTest {

    private ArrivalAndDepartureAirportValidator validator;

    @Before
    public void setup() {
        validator = new ArrivalAndDepartureAirportValidator();
    }

    @Test
    public void testIsValid_ok() {
        assertTrue("Ok", validator.isValid(new ArrivalAndDepartureAirportHolder() {

            @Override
            public String getDepartureAirportCode() {
                return "GOT";
            }

            @Override
            public String getArrivalAirportCode() {
                return "ARN";
            }
        }, null));
    }

    @Test
    public void testIsValid_null() {
        assertFalse("Departure null", validator.isValid(new ArrivalAndDepartureAirportHolder() {

            @Override
            public String getDepartureAirportCode() {
                return null;
            }

            @Override
            public String getArrivalAirportCode() {
                return "ARN";
            }
        }, null));

        assertFalse("Arrival null", validator.isValid(new ArrivalAndDepartureAirportHolder() {

            @Override
            public String getDepartureAirportCode() {
                return "GOT";
            }

            @Override
            public String getArrivalAirportCode() {
                return null;
            }
        }, null));

        assertFalse("Both null", validator.isValid(new ArrivalAndDepartureAirportHolder() {

            @Override
            public String getDepartureAirportCode() {
                return null;
            }

            @Override
            public String getArrivalAirportCode() {
                return null;
            }
        }, null));
    }

}
