package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator implements ConstraintValidator<Latitude, Object> {

    @Override
    public void initialize(Latitude constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        double latitude = 0;

        if (value instanceof String) {
            try {
                latitude = Double.parseDouble((String) value);
            }
            catch (NumberFormatException e) {
                return false;
            }
        }
        else if (value instanceof Integer) {
            latitude = ((Integer) value).doubleValue();
        }
        else if (value instanceof Double) {
            latitude = (Double) value;
        }
        else {
            return false;
        }

        return latitude != 0.0 && latitude >= -90.0 && latitude <= 90.0;
    }
}
