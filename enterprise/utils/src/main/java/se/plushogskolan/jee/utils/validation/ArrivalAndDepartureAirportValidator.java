package se.plushogskolan.jee.utils.validation;

import java.util.logging.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ArrivalAndDepartureAirportValidator implements ConstraintValidator<ArrivalAndDepartureAirport, ArrivalAndDepartureAirportHolder> {

    private static final Logger log = Logger.getLogger(ArrivalAndDepartureAirportValidator.class.getName());

    @Override
    public void initialize(ArrivalAndDepartureAirport constraintAnnotation) {

    }

    @Override
    public boolean isValid(ArrivalAndDepartureAirportHolder value, ConstraintValidatorContext constraintValidatorContext) {

        if (value.getArrivalAirportCode() == null || value.getDepartureAirportCode() == null) {
            return false;
        }

        if (value.getDepartureAirportCode().isEmpty() || value.getArrivalAirportCode().isEmpty()) {
            return false;
        }

        return !value.getArrivalAirportCode().equals(value.getDepartureAirportCode());
    }

}
