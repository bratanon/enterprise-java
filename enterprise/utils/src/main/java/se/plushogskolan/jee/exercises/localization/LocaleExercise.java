package se.plushogskolan.jee.exercises.localization;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class LocaleExercise {

    private final static Logger log = Logger.getLogger(LocaleExercise.class.getName());

    public String sayHello(Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        return messages.getString("sayHello");
    }

    public String sayName(String string, Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        return MessageFormat.format(messages.getString("sayName"), string);
    }

    public String saySomethingEnglish(Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        return messages.getString("saySomethingEnglish");
    }

    public String introduceYourself(int year, String city, String name, Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        return MessageFormat.format(messages.getString("introduction"), name, year, city);
    }

    public String sayApointmentDate(Date date, Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        MessageFormat messageFormat = new MessageFormat(messages.getString("sayApointmentDate"), locale);
        Object[] args = { date };
        return messageFormat.format(args);
    }

    public String sayPrice(double price, Locale locale) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise", locale);
        MessageFormat messageFormat = new MessageFormat(messages.getString("sayPrice"), locale);
        Object[] args = { price };
        return messageFormat.format(args);
    }

    public String sayFractionDigits(double no) {
        final ResourceBundle messages = ResourceBundle.getBundle("localizationexercise");
        return MessageFormat.format(messages.getString("sayFractionDigits"), no);
    }
}
