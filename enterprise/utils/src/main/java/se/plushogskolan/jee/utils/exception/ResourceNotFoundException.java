package se.plushogskolan.jee.utils.exception;

/**
 * A runtime exception that triggers 404 pages.
 *
 * @author Emil Stjerneman
 *
 */
public class ResourceNotFoundException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -4645431599690550067L;

    public ResourceNotFoundException (String message) {
        super(message);
    }
}
