package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongitudeValidator implements ConstraintValidator<Longitude, Object> {

    @Override
    public void initialize(Longitude constraintAnnotation) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        double longitude = 0;

        if (value instanceof String) {
            try {
                longitude = Double.parseDouble((String) value);
            }
            catch (NumberFormatException e) {
                return false;
            }
        }
        else if (value instanceof Integer) {
            longitude = ((Integer) value).doubleValue();
        }
        else if (value instanceof Double) {
            longitude = (Double) value;
        }
        else {
            return false;
        }

        return longitude != 0.0 && longitude >= -180.0 && longitude <= 180.0;
    }

}
