<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:message code="editPlaneType.title.create" var="title"/>
<spring:message code="global.submit" var="submitTitle"/>
<c:if test="${editPlaneTypeBean.planeType.id > 0}">
    <spring:message code="editPlaneType.title.update" var="title"/>
</c:if>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
        <div class="site-wrapper">
            <section class="width50">
                <h1><i class="icon-flight"></i>${title}</h1>

                <form:form commandName="editPlaneTypeBean">
                    <form:hidden path="planeType.id" />

                    <c:if test="${editPlaneTypeBean.planeType.id > 0}">
                    <div class="form-item">
                        <label for=""><spring:message code="global.id" /></label>
                        ${editPlaneTypeBean.planeType.id}
                    </div>
                    </c:if>

                     <div class="form-item">
                        <label for="planeType.code"><spring:message code="flightrequest.code" /></label>
                        <form:input path="planeType.code" />
                        <form:errors path="planeType.code" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="planeType.name"><spring:message code="global.name" /></label>
                        <form:input path="planeType.name" />
                        <form:errors path="planeType.name" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="planeType.maxNoOfPassengers"><spring:message code="flightrequest.noOfPassengers" /></label>
                        <form:input path="planeType.maxNoOfPassengers" size="7" maxlength="3" />
                        <form:errors path="planeType.maxNoOfPassengers" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="planeType.maxRangeKm"><spring:message code="flightrequest.range" /></label>
                        <form:input path="planeType.maxRangeKm" size="7" maxlength="7" />
                        <form:errors path="planeType.maxRangeKm" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="planeType.maxSpeedKmH"><spring:message code="flightrequest.maxSpeed" /></label>
                        <form:input path="planeType.maxSpeedKmH" size="7" maxlength="4" />
                        <form:errors path="planeType.maxSpeedKmH" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="planeType.fuelConsumptionPerKm"><spring:message code="flightrequest.fuelConsumption" /></label>
                        <form:input path="planeType.fuelConsumptionPerKm" size="7" maxlength="3" />
                        <form:errors path="planeType.fuelConsumptionPerKm" cssClass="error" />
                    </div>

                     <div class="form-actions">
                        <input type="submit" name="op" value="${submitTitle}" />
                        <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
                    </div>
                </form:form>
            </section>
        </div>
    </div>
</body>

</html>