<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="editFuelCost.title" /></title>
    <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
        <div class="site-wrapper">
            <section class="width50">
                <h1><i class="icon-fuel"></i><spring:message code="editFuelCost.title" /></h1>

                <form:form commandName="editFuelCostBean">
                    <div class="form-item">
                        <label for="fuelCost"><spring:message code="global.fuelPrice" /></label>
                        <form:input path="fuelCost" />
                        <form:errors path="fuelCost" cssClass="error" />
                    </div>
                    
                    <div class="form-actions">
                        <c:set var="submitText">
                            <spring:message code="global.submit" />
                        </c:set>
                        <input type="submit" name="op" value="${submitText}" />
                        <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
                    </div>
                </form:form>
            </section>

        </div>
    </div>
</body>
</html>