<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<header id="site-header">
    <div class="site-wrapper">
        <nav>
            <ul>
                <li><spring:message code="header.nav.loggedInAs"/><a href="#">Jetbroker-jakten</a></li>
                <li><a href="#"><spring:message code="header.nav.reports"/></a></li>
                <li><a href="#"><spring:message code="header.nav.helpdesk"/></a></li>
                <li><a href="#"><spring:message code="header.nav.logout"/></a></li>
            </ul>
        </nav>
        <h1>
            <a href="<c:url value="/index.html"/>">Jetbroker</a><span>- Plane</span>
        </h1>
    </div>
</header>