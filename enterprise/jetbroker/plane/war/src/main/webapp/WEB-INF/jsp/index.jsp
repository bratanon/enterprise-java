<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
        <div class="site-wrapper">
            <section>
                <h1><i class="icon-flight"></i><spring:message code="global.planeTypes"/></h1>
                <table>
                    <thead>
                        <tr>
                            <th class="id"><spring:message code="global.id" /></th>
                            <th><spring:message code="flightrequest.code" /></th>
                            <th><spring:message code="global.name" /></th>
                            <th><spring:message code="flightrequest.noOfPassengers" /></th>
                            <th><spring:message code="flightrequest.range" /></th>
                            <th><spring:message code="flightrequest.maxSpeed" /></th>
                            <th><spring:message code="flightrequest.fuelConsumption" /></th>
                            <th class="operations"><spring:message code="global.operations" /></th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${planeTypes}" var="planeType">
                        <spring:url value="/editPlaneType/${planeType.id}.html" var="edit" />
                        <tr>
                            <td>${planeType.id}</td>
                            <td>${planeType.code}</td>
                            <td>${planeType.name}</td>
                            <td>${planeType.maxNoOfPassengers}</td>
                            <td>${planeType.maxRangeKm}</td>
                            <td>${planeType.maxSpeedKmH}</td>
                            <td>${planeType.fuelConsumptionPerKm}</td>
                            <td class="operations align-right">
                                <a href="${edit}" class="edit" title="<spring:message code="global.edit"/>"><i class="icon-cog"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </section>

            <section>
                <h1><i class="icon-road"></i><spring:message code="global.airports" /></h1>
                <table>
                    <thead>
                        <tr>
                            <th class="id"><spring:message code="global.id" /></th>
                            <th><spring:message code="airport.code" /></th>
                            <th><spring:message code="global.name" /></th>
                            <th><spring:message code="airport.latitude" /></th>
                            <th><spring:message code="airport.longitude" /></th>
                            <th class="operations"><spring:message code="global.operations" /></th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${airports}" var="airport">
                        <spring:url value="/editAirport/${airport.id}.html" var="edit" />
                        <tr>
                            <td>${airport.id}</td>
                            <td>${airport.code}</td>
                            <td>${airport.name}</td>
                            <td>${airport.latitude}</td>
                            <td>${airport.longitude}</td>
                            <td class="operations align-right">
                                <a href="${edit}" class="edit" title="<spring:message code="global.edit"/>"><i class="icon-cog"></i></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </section>

            <section>
                <h1><i class="icon-fuel"></i><spring:message code="global.fuelPrice" /></h1>
                <spring:url value="/editFuelCost.html" var="edit" />
                <p>
                    <spring:message code="index.current.fuelCost" arguments="${fuelCost}" /> SEK. 
                    <a href="${edit}" class="edit" title="<spring:message code="global.edit"/>"><i class="icon-cog"></i></a>
                </p>
            </section>

        </div>
    </div>
</body>
</html>