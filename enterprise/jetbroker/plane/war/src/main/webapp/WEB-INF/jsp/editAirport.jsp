<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:message code="editAirport.title.create" var="title"/>
<spring:message code="global.submit" var="submitTitle"/>
<c:if test="${editAirportBean.id > 0}">
    <spring:message code="editAirport.title.update" var="title"/>
</c:if>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
        <div class="site-wrapper">
            <section class="width50">
                <h1><i class="icon-road"></i>${title}</h1>

                <form:form commandName="editAirportBean">
                    <form:hidden path="id"/>

                    <c:if test="${editAirportBean.id > 0}">
                    <div class="form-item">
                        <label for=""><spring:message code="global.id" /></label>
                        ${editAirportBean.id}
                    </div>
                    </c:if>

                     <div class="form-item">
                        <label for="code"><spring:message code="airport.code" /></label>
                        <form:input path="code" />
                        <form:errors path="code" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="name"><spring:message code="global.name" /></label>
                        <form:input path="name" />
                        <form:errors path="name" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="latitude"><spring:message code="airport.latitude" /></label>
                        <form:input path="latitude" size="20" maxlength="20" />
                        <form:errors path="latitude" cssClass="error" />
                    </div>

                    <div class="form-item">
                        <label for="longitude"><spring:message code="airport.longitude" /></label>
                        <form:input path="longitude" size="20" maxlength="20"/>
                        <form:errors path="longitude" cssClass="error" />
                    </div>

                     <div class="form-actions">
                        <input type="submit" name="op" value="${submitTitle}" />
                        <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
                    </div>
                </form:form>
            </section>
        </div>
    </div>
</body>

</html>

