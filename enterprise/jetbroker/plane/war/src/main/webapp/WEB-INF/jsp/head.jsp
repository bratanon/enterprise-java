<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<meta charset="utf-8" />
<title><spring:message code="application.title"/></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/reset.css" />
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,900,400italic,700italic,900italic" media="all" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/fontello.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/style/common.css" />
