package se.plushogskolan.jetbroker.plane.mvc;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class EditFuelCostBean {

	@Min(value = 1)
	@Max(value = 50)
	private double fuelCost;

	public double getFuelCost() {
		return fuelCost;
	}

	public void setFuelCost(double fuelCost) {
		this.fuelCost = fuelCost;
	}

}
