package se.plushogskolan.jetbroker.agent.mvc;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jee.utils.exception.ResourceNotFoundException;
import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

@Controller
@RequestMapping("/flightrequest")
public class FlightRequestController {

    private static final Logger log = Logger.getLogger(FlightRequestController.class.getName());

    @Inject
    private FlightRequestService flightRequestService;

    @Inject
    private CustomerService customerService;

    @Inject
    private AirportService airportService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView flightRequestForm() {
        return this.flightRequestForm(0);
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView flightRequestForm(@PathVariable long id) {
        FlightRequestBean bean = new FlightRequestBean();

        boolean isNew = !(id > 0);
        if (!isNew) {
            bean.copyFlightRequestValuesToBean(getFlightRequestService().getFlightRequest(id));
        }

        List<Customer> customers = customerService.getAllCustomers();
        Collections.sort(customers);

        List<Airport> airports = airportService.getAllAirports();
        Collections.sort(airports);

        ModelAndView mav = new ModelAndView("flightRequestForm");
        mav.addObject("isNew", isNew);
        mav.addObject("flightRequestBean", bean);
        mav.addObject("customers", customers);
        mav.addObject("airports", airports);
        return mav;
    }

    @RequestMapping(value = { "/add", "/{id}/edit" }, method = RequestMethod.POST)
    public ModelAndView handleFlightRequestForm(@Valid FlightRequestBean bean, BindingResult errors) {
        boolean isNew = !(bean.getId() > 0);

        // Handle errors.
        if (errors.hasErrors()) {
            List<Customer> customers = customerService.getAllCustomers();
            Collections.sort(customers);

            List<Airport> airports = airportService.getAllAirports();
            Collections.sort(airports);

            ModelAndView mav = new ModelAndView("flightRequestForm");
            mav.addObject("isNew", isNew);
            mav.addObject("flightRequestBean", bean);
            mav.addObject("customers", customers);
            mav.addObject("airports", airports);
            return mav;
        }

        // TODO: Refactor this customer code.
        Customer customer = customerService.getCustomer(bean.getCustomerId());
        if (customer == null) {
            throw new ResourceNotFoundException("Customer not found.");
        }

        if (isNew) {
            FlightRequest flightRequest = new FlightRequest();
            bean.copyBeanValuesToFlightRequest(flightRequest, customer);
            getFlightRequestService().createFlightRequest(flightRequest);
        }
        else {
            FlightRequest flightRequest = getFlightRequestService().getFlightRequest(bean.getId());
            bean.copyBeanValuesToFlightRequest(flightRequest, customer);
            getFlightRequestService().updateFlightRequest(flightRequest);
        }

        return new ModelAndView("redirect:/index.html");
    }

    public FlightRequestService getFlightRequestService() {
        return flightRequestService;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public AirportService getAirportService() {
        return airportService;
    }

}
