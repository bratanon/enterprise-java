package se.plushogskolan.jetbroker.agent.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

@Controller
public class IndexController {

    @Inject
    private FlightRequestService flightRequestService;

    @Inject
    private CustomerService customerService;

    @Inject
    private AirportService airportService;

    @RequestMapping("index")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("index");

        List<FlightRequest> allFlightRequests = getFlightRequestService().getAllFlightRequests();

        List<FlightRequest> unprocessedFlightRequests = new ArrayList<>();
        List<FlightRequest> offeredFlightRequests = new ArrayList<>();
        List<FlightRequest> rejectedFlightRequests = new ArrayList<>();

        for (FlightRequest flightRequest : allFlightRequests) {
            switch (flightRequest.getStatus()) {
                case CREATED:
                case REQUEST_CONFIRMED:
                    unprocessedFlightRequests.add(flightRequest);
                    break;
                case OFFER_RECEIVED:
                    offeredFlightRequests.add(flightRequest);
                    break;
                case REJECTED:
                    rejectedFlightRequests.add(flightRequest);
                    break;
            }
        }

        mav.addObject("customers", getCustomerService().getAllCustomers());

        mav.addObject("unprocessedFlightRequests", unprocessedFlightRequests);
        mav.addObject("offeredFlightRequests", offeredFlightRequests);
        mav.addObject("rejectedFlightRequests", rejectedFlightRequests);

        return mav;
    }

    public FlightRequestService getFlightRequestService() {
        return flightRequestService;
    }

    public void setFlightRequestService(FlightRequestService flightRequestService) {
        this.flightRequestService = flightRequestService;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public AirportService getAirportService() {
        return airportService;
    }

    public void setAirportService(AirportService airportService) {
        this.airportService = airportService;
    }

}
