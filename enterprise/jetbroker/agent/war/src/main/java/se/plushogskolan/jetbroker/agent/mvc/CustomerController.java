package se.plushogskolan.jetbroker.agent.mvc;

import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jee.utils.exception.ResourceNotFoundException;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    private static final Logger log = Logger.getLogger(CustomerController.class.getName());

    @Inject
    private CustomerService customerService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView customerForm() {
        CustomerBean bean = new CustomerBean();

        ModelAndView mav = new ModelAndView("customerForm");
        mav.addObject("isNew", true);
        mav.addObject("customerBean", bean);

        return mav;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView handleCustomerAdd(@Valid CustomerBean bean, BindingResult errors, Locale locale) {
        // Handle errors.
        if (errors.hasErrors()) {
            ModelAndView mav = new ModelAndView("customerForm");
            mav.addObject("isNew", true);
            mav.addObject("customerBean", bean);
            return mav;
        }

        if (bean.getFirstName().equals("i18n")) {
            log.info(messageSource.getMessage("i18n.test.message", null, locale));
        }

        Customer customer = new Customer();
        bean.copyBeanValuesToCustomer(customer);
        getCustomerService().createCustomer(customer);

        return new ModelAndView("redirect:/index.html");
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView customerForm(@PathVariable long id) {
        Customer customer = getCustomerService().getCustomer(id);

        if (customer == null) {
            throw new ResourceNotFoundException(String.format("Customer not found."));
        }

        CustomerBean bean = new CustomerBean();
        bean.copyCustomerValuesToBean(customer);

        ModelAndView mav = new ModelAndView("customerForm");
        mav.addObject("isNew", false);
        mav.addObject("customerBean", bean);

        return mav;
    }

    @RequestMapping(value = "/{customerId}/edit", method = RequestMethod.POST)
    public ModelAndView handleCustomerEdit(@Valid CustomerBean bean, BindingResult errors, Locale locale) {
        // Handle errors.
        if (errors.hasErrors()) {
            ModelAndView mav = new ModelAndView("customerForm");
            mav.addObject("isNew", false);
            mav.addObject("customerBean", bean);
            return mav;
        }

        if (bean.getFirstName().equals("i18n")) {
            log.info(messageSource.getMessage("i18n.test.message", null, locale));
        }

        Customer customer = getCustomerService().getCustomer(bean.getId());
        bean.copyBeanValuesToCustomer(customer);
        getCustomerService().updateCustomer(customer);

        return new ModelAndView("redirect:/index.html");
    }

    @RequestMapping(value = "/{customerId}/remove", method = RequestMethod.GET)
    public ModelAndView customerConfirmForm(@PathVariable long customerId) {
        Customer customer = getCustomerService().getCustomer(customerId);

        if (customer == null) {
            throw new ResourceNotFoundException(String.format("Customer not found."));
        }

        ModelAndView mav = new ModelAndView("customerDeleteConfirm");
        mav.addObject("customer", customer);

        return mav;
    }

    @RequestMapping(value = "/{customerId}/remove", method = RequestMethod.POST)
    public ModelAndView handleCustomerDelete(@PathVariable long customerId) {
        Customer customer = getCustomerService().getCustomer(customerId);

        if (customer == null) {
            throw new ResourceNotFoundException(String.format("Customer not found."));
        }

        getCustomerService().removeCustomer(customer.getId());

        return new ModelAndView("redirect:/index.html");
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
}
