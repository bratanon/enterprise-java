package se.plushogskolan.jetbroker.agent.mvc;

import java.util.logging.Logger;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

@ArrivalAndDepartureAirport
public class FlightRequestBean implements ArrivalAndDepartureAirportHolder {

    private final static Logger log = Logger.getLogger(FlightRequestBean.class.getName());

    private long id;

    @NotNull(message = "{validation.flightRequest.customer.missing}")
    private Long customerId;

    @NotBlank(message = "{validation.flightRequest.departureAirportCode.missing}")
    private String departureAirportCode;

    @NotBlank(message = "{validation.flightRequest.arrivalAirportCode.missing}")
    private String arrivalAirportCode;

    @NotNull(message = "{validation.flightRequest.passengers.missing}")
    @Range(min = 1, max = 500)
    private Integer passengers;

    @NotNull(message = "{validation.flightRequest.date.missing}")
    @DateTimeFormat(pattern = "YYYY-MM-dd")
    private DateTime date;

    @NotNull(message = "{validation.flightRequest.hour.missing}")
    private Integer hour;

    @NotNull(message = "{validation.flightRequest.minute.missing}")
    private Integer minute;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    @Override
    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public void copyBeanValuesToFlightRequest(FlightRequest flightRequest, Customer customer) {
        flightRequest.setId(getId());
        flightRequest.setCustomer(customer);
        flightRequest.setPassengers(getPassengers());
        flightRequest.setDepartureAirportCode(getDepartureAirportCode());
        flightRequest.setArrivalAirportCode(getArrivalAirportCode());

        MutableDateTime date = getDate().toMutableDateTime();
        date.setHourOfDay(getHour());
        date.setMinuteOfHour(getMinute());
        flightRequest.setDepartureDate(date.toDateTime());
    }

    public void copyFlightRequestValuesToBean(FlightRequest flightRequest) {
        setId(flightRequest.getId());
        setCustomerId(flightRequest.getCustomer().getId());
        setDepartureAirportCode(flightRequest.getDepartureAirportCode());
        setArrivalAirportCode(flightRequest.getArrivalAirportCode());
        setPassengers(flightRequest.getPassengers());
        setDate(flightRequest.getDepartureDate());
        setHour(flightRequest.getDepartureDate().getHourOfDay());
        setMinute(flightRequest.getDepartureDate().getMinuteOfHour());
    }

}
