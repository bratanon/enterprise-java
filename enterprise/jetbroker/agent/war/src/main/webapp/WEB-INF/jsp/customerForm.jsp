<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="customer.new" var="title"/>
<spring:message code="customer.new.submit" var="submitTitle"/>
<c:if test="${!isNew}">
    <spring:message code="customer.update" var="title"/>
    <spring:message code="customer.update.submit" var="submitTitle"/>
</c:if>


<!DOCTYPE html>
<html>
<head>
  <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
      <div class="site-wrapper">
        <section class="width50">
          <h1><i class="icon-user"></i>${title}</h1>

          <form:form commandName="customerBean" method="post">
            <div class="form-item">
                <label for="firstName"><spring:message code="customer.firstName" /></label>
                <form:input path="firstName" />
                <form:errors path="firstName" cssClass="error" />
            </div>

            <div class="form-item">
                <label for="lastName"><spring:message code="customer.lastName" /></label>
                <form:input path="lastName" />
                <form:errors path="lastName" cssClass="error" />
            </div>

            <div class="form-item">
                <label for="email"><spring:message code="customer.email" /></label>
                <form:input path="email" />
                <form:errors path="email" cssClass="error" />
            </div>

            <div class="form-item">
                <label for=""><spring:message code="customer.company" /></label>
                <form:input path="company" />
                <form:errors path="company" cssClass="error" />
            </div>

            <div class="form-actions">
                <input type="submit" name="op" value="${submitTitle}" />
                <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
            </div>

            <c:if test="${!isNew}">
                <form:hidden path="id" />
            </c:if>

          </form:form>
        </section>
      </div>
    </div>
</body>

</html>