<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<spring:message code="flightRequest.new" var="title"/>
<spring:message code="flightRequest.new.submit" var="submitTitle"/>
<spring:message code="flightRequest.selectCostomer" var="selectCostomer" />
<spring:message code="flightRequest.selectAirport" var="selectAirport" />
<c:if test="${!isNew}">
    <spring:message code="flightRequest.update" var="title"/>
    <spring:message code="flightRequest.update.submit" var="submitTitle"/>
</c:if>

<!DOCTYPE html>
<html>
<head>
  <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
      <div class="site-wrapper">
        <section class="width50">
          <h1><i class="icon-inbox"></i>${title}</h1>

          <form:form commandName="flightRequestBean" method="post">

            <div class="form-item">
                <label for="customer"><spring:message code="flightRequest.customer" /></label>
                <form:select  path="customerId">
                    <c:if test="${isNew}">
                        <form:option value="" label="${selectCostomer}"/>
                    </c:if>
                    <form:options items="${customers}" itemValue="id" itemLabel="fullName"/>
                </form:select>
                <form:errors path="customerId" cssClass="error"/>
            </div>

            <div class="form-item">
                <label for="departureAirport"><spring:message code="flightRequest.departureAirport" /></label>
                <form:select  path="departureAirportCode">
                    <c:if test="${isNew}">
                        <form:option value="" label="${selectAirport}"/>
                    </c:if>
                    <form:options items="${airports}" itemValue="code" itemLabel="fullName"/>
                </form:select>
                <form:errors path="departureAirportCode" cssClass="error" />
            </div>

            <div class="form-item">
                <label for="arrivalAirportCode"><spring:message code="flightRequest.arrivalAirport" /></label>
                <form:select  path="arrivalAirportCode">
                    <c:if test="${isNew}">
                        <form:option value="" label="${selectAirport}"/>
                    </c:if>
                    <form:options items="${airports}" itemValue="code" itemLabel="fullName"/>
                </form:select>
                <form:errors path="arrivalAirportCode" cssClass="error"/>
            </div>
            
            <div class="form-item">
                <label for="passengers"><spring:message code="flightRequest.noOfPassengers" /></label>
                <form:input path="passengers" />
                <form:errors path="passengers" cssClass="error"/>
            </div>
            
            <div class="form-item">
                <label for="date"><spring:message code="flightRequest.departureDate" /></label>
                <form:input path="date" class="date" />
                <form:errors path="date" cssClass="error"/>
            </div>

            <div class="form-item hour-minute">
                <label for="hour"><spring:message code="flightRequest.departureTime" /></label>
                <form:select path="hour">
                    <c:if test="${isNew}">
                        <form:option value="" label=""></form:option>
                    </c:if>
                    <c:forEach var="i" begin="0" end="23">
                        <c:if test="${i < 10}">
                            <c:set var="i" value="0${i}" />
                        </c:if>
                        <form:option value="${i}" label="${i}"></form:option>
                    </c:forEach>
                </form:select>
                <form:select path="minute" >
                    <c:if test="${isNew}">
                        <form:option value="" label=""></form:option>
                    </c:if>
                    <c:forEach var="i" begin="0" end="59" step="5">
                         <c:if test="${i < 10}">
                            <c:set var="i" value="0${i}" />
                        </c:if>
                        <form:option value="${i}" label="${i}"></form:option>
                    </c:forEach>
                </form:select>
                <form:errors path="hour" cssClass="error"/>
                <form:errors path="minute" cssClass="error"/>
            </div>
            
            <form:errors path="" cssClass="error"/>
 
            <div class="form-actions">
                <input type="submit" name="op" value="${submitTitle}" />
                <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
            </div>

            <c:if test="${!isNew}">
                <form:hidden path="id" />
            </c:if>

          </form:form>
        </section>
      </div>
    </div>
</body>

</html>