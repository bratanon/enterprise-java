<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<section>
    <h1><i class="icon-inbox"></i><spring:message code="flightRequest.title" /></h1>
    <table>
        <caption><spring:message code="flightRequest.unprocessedFlightRequests" arguments="${unprocessedFlightRequests.size()}" /></caption>
        <thead>
            <tr>
                <th class="id"><spring:message code="global.id" /></th>
                <th><spring:message code="flightRequest.status" /></th>
                <th><spring:message code="flightRequest.departureAirport" /></th>
                <th><spring:message code="flightRequest.arrivalAirport" /></th>
                <th><spring:message code="flightRequest.date" /></th>
                <th><spring:message code="flightRequest.customer" /></th>
                <th><spring:message code="flightRequest.passengers" /></th>
                <th class="operations"><spring:message code="global.operations" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${unprocessedFlightRequests}" var="flightRequest">
                <spring:url value="/flightrequest/${flightRequest.id}/edit.html" var="edit" />
                <tr>
                    <td><c:out value="${flightRequest.id}"/></td>
                    <td><c:out value="${flightRequest.status.title}"/></td>
                    <td><c:out value="${flightRequest.departureAirportCode}"/></td>
                    <td><c:out value="${flightRequest.arrivalAirportCode}"/></td>
                    <td><c:out value="${flightRequest.formatedDate}"/></td>
                    <td><c:out value="${flightRequest.customer.fullName}"/></td>
                    <td><c:out value="${flightRequest.passengers}"/></td>
                    <td class="operations align-right">
                        <a href="${edit}" class="edit" title="<spring:message code="global.edit" />"><i class="icon-cog"></i></a>
                    </td>
                </tr>
            </c:forEach>
            <c:if test="${empty unprocessedFlightRequests}">
                <tr><td colspan="8"><spring:message code="flightRequest.noUnprocessed" /></td></tr>
            </c:if>
        <tbody>
    </table>
    
    <table>
        <caption><spring:message code="flightRequest.offeredFlightRequests" arguments="${offeredFlightRequests.size()}" /></caption>
        <thead>
            <tr>
                <th class="id"><spring:message code="global.id" /></th>
                <th><spring:message code="flightRequest.status" /></th>
                <th><spring:message code="flightRequest.departureAirport" /></th>
                <th><spring:message code="flightRequest.arrivalAirport" /></th>
                <th><spring:message code="flightRequest.date" /></th>
                <th><spring:message code="flightRequest.customer" /></th>
                <th><spring:message code="flightRequest.passengers" /></th>
                <th><spring:message code="flightRequest.planeType" /></th>
                <th><spring:message code="flightRequest.offeredPrice" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${offeredFlightRequests}" var="flightRequest">
                <tr>
                    <td><c:out value="${flightRequest.id}"/></td>
                    <td><c:out value="${flightRequest.status.title}"/></td>
                    <td><c:out value="${flightRequest.departureAirportCode}"/></td>
                    <td><c:out value="${flightRequest.arrivalAirportCode}"/></td>
                    <td><c:out value="${flightRequest.formatedDate}"/></td>
                    <td><c:out value="${flightRequest.customer.fullName}"/></td>
                    <td><c:out value="${flightRequest.passengers}"/></td>
                    <td>[PLANETYPE]</td>
                    <td>[PRICE]</td>
                </tr>
            </c:forEach>
            <c:if test="${empty offeredFlightRequests}">
                <tr><td colspan="9"><spring:message code="flightRequest.noOffered" /></td></tr>
            </c:if>
        <tbody>
    </table>
    
    <table>
        <caption><spring:message code="flightRequest.rejectedFlightRequests" arguments="${rejectedFlightRequests.size()}" /></caption>
        <thead>
            <tr>
                <th class="id"><spring:message code="global.id" /></th>
                <th><spring:message code="flightRequest.status" /></th>
                <th><spring:message code="flightRequest.departureAirport" /></th>
                <th><spring:message code="flightRequest.arrivalAirport" /></th>
                <th><spring:message code="flightRequest.date" /></th>
                <th><spring:message code="flightRequest.customer" /></th>
                <th><spring:message code="flightRequest.passengers" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${rejectedFlightRequests}" var="flightRequest">
                <tr>
                    <td><c:out value="${flightRequest.id}"/></td>
                    <td><c:out value="${flightRequest.status.title}"/></td>
                    <td><c:out value="${flightRequest.departureAirportCode}"/></td>
                    <td><c:out value="${flightRequest.arrivalAirportCode}"/></td>
                    <td><c:out value="${flightRequest.formatedDate}"/></td>
                    <td><c:out value="${flightRequest.customer.fullName}"/></td>
                    <td><c:out value="${flightRequest.passengers}"/></td>
                </tr>
            </c:forEach>
            <c:if test="${empty rejectedFlightRequests}">
                <tr><td colspan="7"><spring:message code="flightRequest.noRejected" /></td></tr>
            </c:if>
        <tbody>
    </table>
</section>