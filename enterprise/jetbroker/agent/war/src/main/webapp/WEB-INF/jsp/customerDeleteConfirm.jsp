<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
        <div class="site-wrapper">
            <section>
                <h1><i class="icon-remove"></i>Remove ${customer.fullName}?</h1>
                <p>Evil things might happen. This action can not be undone.</p>
                <form:form method="post">
                    <div class="form-actions">
                        <input type="submit" name="op" value="Yes, I'm sure." />
                        <a href="<c:url value="/index.html"/>">Cancel</a>
                    </div>
                </form:form>
            </section>
        </div>
    </div>
</body>
</html>