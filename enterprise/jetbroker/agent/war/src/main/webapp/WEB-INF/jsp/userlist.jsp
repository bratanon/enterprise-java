<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<section>
    <h1><i class="icon-users"></i><spring:message code="customer.title"/></h1>
    <table>
        <thead>
            <tr>
                <th class="id"><spring:message code="global.id"/></th>
                <th><spring:message code="customer.firstName"/></th>
                <th><spring:message code="customer.lastName"/></th>
                <th><spring:message code="customer.company"/></th>
                <th class="operations"><spring:message code="global.operations"/></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${customers}" var="customer">
                <spring:url value="/customer/${customer.id}/edit.html" var="edit" />
                <spring:url value="/customer/${customer.id}/remove.html" var="remove" />
                <tr>
                    <td><c:out value="${customer.id}"/></td>
                    <td><c:out value="${customer.firstName}"/></td>
                    <td><c:out value="${customer.lastName}"/></td>
                    <td><c:out value="${customer.company}"/></td>
                    <td class="operations align-right">
                        <a href="${edit}" class="edit" title="<spring:message code="global.edit"/>"><i class="icon-cog"></i></a>
                        <a href="${remove}" class="remove" title="<spring:message code="global.remove"/>"><i class="icon-remove"></i></a>
                    </td>
                </tr>
            </c:forEach>
            <c:if test="${empty customers}">
                <tr><td colspan="5"><spring:message code="customer.noCustomer"/></td></tr>
            </c:if>
        <tbody>
    </table>
</section>
