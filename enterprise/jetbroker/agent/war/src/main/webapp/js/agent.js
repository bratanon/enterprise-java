(function($) {

    $(function() {
        $("input.date").datepicker({ dateFormat: "yy-mm-dd" });
    });

})(jQuery);