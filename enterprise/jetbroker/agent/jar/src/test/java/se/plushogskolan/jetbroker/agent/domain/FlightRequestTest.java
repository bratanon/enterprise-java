package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture.ValidCustomerBuilder;
import se.plushogskolan.jetbroker.TestFixture.ValidFlightRequestBuilder;

public class FlightRequestTest {

    private PropertyValidator<FlightRequest> propertyValidator;

    Customer customer;

    @Before
    public void setup() {
        propertyValidator = new PropertyValidator<FlightRequest>();
        customer = new ValidCustomerBuilder().build();
    }

    @Test
    public void testCreateFlightRequest() {
        FlightRequest flightRequest = new FlightRequest();
        assertEquals("Status created is default set.", FlightRequest.Status.CREATED, flightRequest.getStatus());
    }

    @Test
    public void testValidationCustomerNull() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(null).build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        propertyValidator.assertPropertyIsInvalid("customer", violations);
    }

    @Test
    public void testValidationCustomerOk() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(customer).build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        assertTrue("No validation errors for customer", violations.isEmpty());
    }

    @Test
    public void testValidationPassengersUnderMin() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(customer).passengers(0).build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        propertyValidator.assertPropertyIsInvalid("passengers", violations);
    }

    @Test
    public void testValidationPassengersOverMax() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(customer).passengers(501).build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        propertyValidator.assertPropertyIsInvalid("passengers", violations);
    }

    @Test
    public void testValidationPassengersInSpan() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(customer).passengers(250).build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        assertTrue("No validation errors for passengers", violations.isEmpty());
    }

    @Test
    public void testValidationEqualAirports() {
        FlightRequest flightRequest = new ValidFlightRequestBuilder(customer).arrivalAirportCode("GOT").departureAirportCode("GOT").build();
        Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(flightRequest);
        propertyValidator.assertPropertyIsInvalid("", violations);

        flightRequest.setArrivalAirportCode("ARN");
        violations = getValidator().validate(flightRequest);
        assertTrue("No validation errors for arrival and departure", violations.isEmpty());
    }

    private Validator getValidator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
