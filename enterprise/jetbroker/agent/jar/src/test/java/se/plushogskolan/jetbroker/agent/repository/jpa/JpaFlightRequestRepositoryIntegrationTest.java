package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

    @Inject
    private JpaFlightRequestRepository repo;

    @Inject
    private CustomerService customerService;

    private Customer customer;

    @Override
    protected JpaFlightRequestRepository getRepository() {
        return repo;
    }

    @Before
    public void setup() {
        customer = customerService.createCustomer(new TestFixture.ValidCustomerBuilder().build());
    }

    @Override
    protected FlightRequest getEntity1() {
        return new TestFixture.ValidFlightRequestBuilder(customer).build();
    }

    @Override
    protected FlightRequest getEntity2() {
        return new TestFixture.ValidFlightRequestBuilder(customer).status(Status.REQUEST_CONFIRMED).build();
    }

    @Test
    public void testGetAllFlightRequests() {
        repo.persist(getEntity1());
        repo.persist(getEntity2());
        assertEquals("List is correct size", 2, repo.getAllFlightRequests().size());
    }

    @Test
    public void testGetAllFlightRequestsStatus() {
        FlightRequest entity1 = repo.findById(repo.persist(getEntity1()));
        FlightRequest entity2 = repo.findById(repo.persist(getEntity2()));
        assertEquals("List is correct size", 1, repo.getAllFlightRequests(Status.REQUEST_CONFIRMED).size());
        assertFalse(repo.getAllFlightRequests(Status.REQUEST_CONFIRMED).contains(entity1));
        assertTrue(repo.getAllFlightRequests(Status.REQUEST_CONFIRMED).contains(entity2));
    }

    @Test
    public void testCreateAndFindFlightRequest() {
        DateTime departureDate = DateTime.now();

        FlightRequest flightRequest = getEntity1();
        flightRequest.setDepartureDate(departureDate);

        FlightRequest savedFlightRequest = repo.findById(repo.persist(flightRequest));

        assertEquals("Customer is correct", getEntity1().getCustomer(), savedFlightRequest.getCustomer());
        assertEquals("Departure date is correct", departureDate, savedFlightRequest.getDepartureDate());
        assertEquals("Status is correct", Status.CREATED, savedFlightRequest.getStatus());
    }

    @Test(expected = Exception.class)
    public void testValidation() {
        FlightRequest flight_request = new TestFixture.ValidFlightRequestBuilder(null).build();
        getRepository().persist(flight_request);
    }

}
