package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture.ValidCustomerBuilder;

public class CustomerTest {

    PropertyValidator<Customer> propertyValidator;

    @Before
    public void setup() {
        propertyValidator = new PropertyValidator<Customer>();
    }

    @Test
    public void testEmptyConstrctor() {
        Customer customer = new Customer();

        assertEquals(0L, customer.getId());
        assertNull(customer.getFirstName());
        assertNull(customer.getLastName());
        assertNull(customer.getEmail());
        assertNull(customer.getCompany());
    }

    @Test
    public void test2ParamConstrctor() {
        Customer customer = new Customer("John", "Doe");

        assertEquals(0L, customer.getId());
        assertEquals("John", customer.getFirstName());
        assertEquals("Doe", customer.getLastName());
        assertNull(customer.getEmail());
        assertNull(customer.getCompany());
    }

    @Test
    public void test4ParamConstrctor() {
        Customer customer = new Customer("John", "Doe", "john@example.com", "Acme");

        assertEquals(0L, customer.getId());
        assertEquals("John", customer.getFirstName());
        assertEquals("Doe", customer.getLastName());
        assertEquals("john@example.com", customer.getEmail());
        assertEquals("Acme", customer.getCompany());
    }

    @Test
    public void testValidationFirstNameEmpty() {
        Customer customer = new ValidCustomerBuilder().firstName("").build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("firstName", violations);
    }

    @Test
    public void testValidationFirstNameNull() {
        Customer customer = new ValidCustomerBuilder().firstName(null).build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("firstName", violations);
    }

    @Test
    public void testValidationFirstNameOk() {
        Customer customer = new ValidCustomerBuilder().build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        assertTrue("No validation errors for firstName", violations.isEmpty());
    }

    @Test
    public void testValidationLastNameEmpty() {
        Customer customer = new ValidCustomerBuilder().lastName("").build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("lastName", violations);
    }

    @Test
    public void testValidationLastNameNull() {
        Customer customer = new ValidCustomerBuilder().lastName(null).build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("lastName", violations);
    }

    @Test
    public void testValidationLastNameOk() {
        Customer customer = new ValidCustomerBuilder().build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        assertTrue("No validation errors for lastName", violations.isEmpty());
    }

    @Test
    public void testValidationEmailEmpty() {
        Customer customer = new ValidCustomerBuilder().email("").build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("email", violations);
    }

    @Test
    public void testValidationEmailNull() {
        Customer customer = new ValidCustomerBuilder().email(null).build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("email", violations);
    }

    @Test
    public void testValidationEmailInvalid() {
        Customer customer = new ValidCustomerBuilder().email("invalid_mail").build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        propertyValidator.assertPropertyIsInvalid("email", violations);
    }

    @Test
    public void testValidationEmailOk() {
        Customer customer = new ValidCustomerBuilder().build();
        Set<ConstraintViolation<Customer>> violations = getValidator().validate(customer);
        assertTrue("No validation errors for email", violations.isEmpty());
    }

    private Validator getValidator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }

}
