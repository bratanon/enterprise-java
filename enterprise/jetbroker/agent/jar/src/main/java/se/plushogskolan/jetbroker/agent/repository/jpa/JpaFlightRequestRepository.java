package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

/**
 * This class is used to get flight request entities using JPA.
 *
 * @author Emil Stjerneman
 *
 */
public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

    @SuppressWarnings("unchecked")
    @Override
    public List<FlightRequest> getAllFlightRequests() {
        return em.createQuery("SELECT f FROM FlightRequest f").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FlightRequest> getAllFlightRequests(Status status) {
        Query query = em.createQuery("SELECT f FROM FlightRequest f WHERE status = :status");
        query.setParameter("status", status);
        return query.getResultList();
    }
}
