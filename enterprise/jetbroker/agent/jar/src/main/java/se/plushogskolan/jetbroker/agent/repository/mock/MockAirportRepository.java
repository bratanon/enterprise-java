package se.plushogskolan.jetbroker.agent.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepository;

/**
 * Mocked implementation of airport repository.
 *
 * @author Emil Stjerneman
 *
 */
public class MockAirportRepository implements AirportRepository {

    private Map<String, Airport> airports = new HashMap<String, Airport>();

    public MockAirportRepository () {
        airports.put("GOT", new Airport("GOT", "Göteborg - Landvetter"));
        airports.put("GSE", new Airport("GSE", "Göteborg - City Airport"));
        airports.put("ARN", new Airport("ARN", "Stockholm - Arlanda"));
    }

    @Override
    public Airport getAirportByCode(String code) {
        return airports.get(code);
    }

    @Override
    public List<Airport> getAllAirports() {
        return new ArrayList<Airport>(airports.values());
    }
}
