package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;

/**
 * Flight request service interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface FlightRequestService {

    /**
     * Creates a flight request.
     *
     * @param flightRequest
     *            a valid flight request entity.
     * @return the saved flight request entity.
     */
    FlightRequest createFlightRequest(FlightRequest flightRequest);

    /**
     * Updates a flight request.
     *
     * @param flightRequest
     *            a flight request with updated values.
     */
    void updateFlightRequest(FlightRequest flightRequest);

    /**
     * Gets a flight request by the given id.
     *
     * @param id
     *            the flight request id.
     * @return a flight request by the given id or null if none is found.
     */
    FlightRequest getFlightRequest(long id);

    /**
     * Gets a list of flight requests regardless of status.
     *
     * @return a list of flight requests or null if none is found.
     */
    List<FlightRequest> getAllFlightRequests();

    /**
     * Gets a list of flight requests with a given status.
     *
     * @param status
     *            the status.
     * @return a list of flight requests with a given status.
     */
    List<FlightRequest> getAllFlightRequests(Status status);

    /**
     * Removed a flight request with a given id.
     *
     * @param id
     *            the flight request id to remove.
     */
    void removeFlightRequest(long id);
}
