package se.plushogskolan.jetbroker.agent.domain;

/**
 * Class representing an airport.
 *
 * The data is fetched from another application, so we don't persists airports
 * in this application.
 *
 * @author Emil Stjerneman
 *
 */
public class Airport implements Comparable<Airport> {

    private String code;
    private String name;

    public Airport () {}

    public Airport (String code, String name) {
        setCode(code);
        setName(name);
    }

    public String getCode() {
        return code;
    }

    private void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return String.format("%s (%s)", getName(), getCode().toUpperCase());
    }

    @Override
    public int compareTo(Airport o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    @Override
    public String toString() {
        return "Airport [code=" + code + ", name=" + name + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Airport)) {
            return false;
        }
        Airport other = (Airport) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        }
        else if (!code.equals(other.code)) {
            return false;
        }
        return true;
    }

}
