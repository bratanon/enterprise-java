package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.AirportCode;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

/**
 * Class representing a flight request entity.
 *
 * @author Emil Stjerneman
 *
 */
@Entity
@ArrivalAndDepartureAirport
public class FlightRequest implements IdHolder, ArrivalAndDepartureAirportHolder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @ManyToOne
    // TODO: Test if null is valid.
    private Customer customer;

    @Range(min = 1, max = 500)
    private int passengers;

    @NotBlank
    @AirportCode
    private String departureAirportCode;

    @NotBlank
    @AirportCode
    private String arrivalAirportCode;

    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime departureDate;

    @NotNull
    private Status status;

    private double price;

    private String planeCode;

    /**
     * Creates a new flight request instance with the status of
     * <code>Status.CREATED</code>.
     *
     * @see Status
     */
    public FlightRequest () {
        setStatus(Status.CREATED);
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    @Override
    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    @Override
    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public DateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateTime departureDate) {
        this.departureDate = departureDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPlaneCode() {
        return planeCode;
    }

    public void setPlaneCode(String planeCode) {
        this.planeCode = planeCode;
    }

    public String getFormatedDate() {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("YYYY-MMM-dd HH:mm");
        return dateFormatter.print(getDepartureDate());
    }

    @Override
    public String toString() {
        return "FlightRequest [id=" + id + ", customer=" + customer + ", passengers=" + passengers + ", departureAirportCode=" + departureAirportCode + ", arrivalAirportCode=" + arrivalAirportCode + ", departureDate=" + departureDate + ", status=" + status + ", price=" + price + ", planeCode=" + planeCode + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((arrivalAirportCode == null) ? 0 : arrivalAirportCode.hashCode());
        result = prime * result + ((customer == null) ? 0 : customer.hashCode());
        result = prime * result + ((departureAirportCode == null) ? 0 : departureAirportCode.hashCode());
        result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
        result = prime * result + passengers;
        result = prime * result + ((planeCode == null) ? 0 : planeCode.hashCode());
        long temp;
        temp = Double.doubleToLongBits(price);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FlightRequest)) {
            return false;
        }
        FlightRequest other = (FlightRequest) obj;
        if (arrivalAirportCode == null) {
            if (other.arrivalAirportCode != null) {
                return false;
            }
        }
        else if (!arrivalAirportCode.equals(other.arrivalAirportCode)) {
            return false;
        }
        if (customer == null) {
            if (other.customer != null) {
                return false;
            }
        }
        else if (!customer.equals(other.customer)) {
            return false;
        }
        if (departureAirportCode == null) {
            if (other.departureAirportCode != null) {
                return false;
            }
        }
        else if (!departureAirportCode.equals(other.departureAirportCode)) {
            return false;
        }
        if (departureDate == null) {
            if (other.departureDate != null) {
                return false;
            }
        }
        else if (!departureDate.equals(other.departureDate)) {
            return false;
        }
        if (passengers != other.passengers) {
            return false;
        }
        if (planeCode == null) {
            if (other.planeCode != null) {
                return false;
            }
        }
        else if (!planeCode.equals(other.planeCode)) {
            return false;
        }
        if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price)) {
            return false;
        }
        return true;
    }

    public enum Status {
        CREATED("New"), REQUEST_CONFIRMED("Request confirmed"), OFFER_RECEIVED("Offer received"), REJECTED("Rejected");

        private String title;

        Status (String title) {
            this.title = title;
        }

        public String getTitle() {
            return this.title;
        }
    };

}
