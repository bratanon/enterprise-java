package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Customer repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface CustomerRepository extends BaseRepository<Customer> {

    /**
     * Gets all customers.
     *
     * @return a list of customers.
     */
    List<Customer> getAllCustomers();

}
