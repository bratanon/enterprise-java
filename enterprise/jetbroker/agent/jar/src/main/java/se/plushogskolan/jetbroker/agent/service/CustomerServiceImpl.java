package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

/**
 * The default customer service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class CustomerServiceImpl implements CustomerService {

    @Inject
    private CustomerRepository customerRepo;

    @Override
    public Customer createCustomer(Customer customer) {
        long customerId = getCustomerRepo().persist(customer);
        return getCustomer(customerId);
    }

    @Override
    public void updateCustomer(Customer customer) {
        getCustomerRepo().update(customer);

    }

    @Override
    public Customer getCustomer(long id) {
        return getCustomerRepo().findById(id);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return getCustomerRepo().getAllCustomers();
    }

    @Override
    public void removeCustomer(long id) {
        getCustomerRepo().remove(getCustomer(id));
    }

    public CustomerRepository getCustomerRepo() {
        return customerRepo;
    }

    public void setCustomerRepo(CustomerRepository customerRepo) {
        this.customerRepo = customerRepo;
    }

}
