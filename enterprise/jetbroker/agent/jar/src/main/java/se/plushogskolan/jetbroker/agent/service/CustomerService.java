package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Customer service interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface CustomerService {

    /**
     * Creates a customer.
     *
     * @param customer
     *            a valid customer entity.
     * @return the saved customer entity.
     */
    Customer createCustomer(Customer customer);

    /**
     * Updates a customer.
     *
     * @param customer
     *            a customer with updated values.
     */
    void updateCustomer(Customer customer);

    /**
     * Gets a customer by the given id.
     *
     * @param id
     *            the customer id.
     * @return a customer by the given id or null if none is found.
     */
    Customer getCustomer(long id);

    /**
     * Gets a list of customers.
     *
     * @return a list of customers or null if none is found.
     */
    List<Customer> getAllCustomers();

    /**
     * Removed a customer with a given id.
     *
     * @param id
     *            the customer id to remove.
     */
    void removeCustomer(long id);

}
