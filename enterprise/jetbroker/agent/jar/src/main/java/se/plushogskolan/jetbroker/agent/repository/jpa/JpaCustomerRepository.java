package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

/**
 * This class is used to get customer entities using JPA.
 * 
 * @author Emil Stjerneman
 *
 */
public class JpaCustomerRepository extends JpaRepository<Customer> implements CustomerRepository {

    @Override
    @SuppressWarnings("unchecked")
    public List<Customer> getAllCustomers() {
        return em.createQuery("SELECT c FROM Customer c").getResultList();
    }
}
