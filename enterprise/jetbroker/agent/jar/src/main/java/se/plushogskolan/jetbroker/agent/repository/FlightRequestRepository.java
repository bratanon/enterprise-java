package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;

/**
 * Flight request repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface FlightRequestRepository extends BaseRepository<FlightRequest> {

    /**
     * Gets all flight requests regardless of status.
     *
     * @return a list of flight requests.
     */
    List<FlightRequest> getAllFlightRequests();

    /**
     * Gets all flight requests with a given status.
     *
     * @param status
     *            the status.
     * @return a list of flight requests with a given status.
     */
    List<FlightRequest> getAllFlightRequests(Status status);

}
