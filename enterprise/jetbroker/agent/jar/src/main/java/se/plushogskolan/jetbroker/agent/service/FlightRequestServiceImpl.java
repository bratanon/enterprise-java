package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

/**
 * The default flight request service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

    @Inject
    private FlightRequestRepository flightRequestRepo;

    @Override
    public FlightRequest createFlightRequest(FlightRequest flightRequest) {
        long id = getFlightRequestRepo().persist(flightRequest);
        return getFlightRequest(id);
    }

    @Override
    public void updateFlightRequest(FlightRequest flightRequest) {
        getFlightRequestRepo().update(flightRequest);
    }

    @Override
    public FlightRequest getFlightRequest(long id) {
        return getFlightRequestRepo().findById(id);
    }

    @Override
    public List<FlightRequest> getAllFlightRequests() {
        return getFlightRequestRepo().getAllFlightRequests();
    }

    @Override
    public List<FlightRequest> getAllFlightRequests(Status status) {
        return getFlightRequestRepo().getAllFlightRequests(status);
    }

    @Override
    public void removeFlightRequest(long id) {
        getFlightRequestRepo().remove(getFlightRequest(id));
    }

    public FlightRequestRepository getFlightRequestRepo() {
        return flightRequestRepo;
    }

    public void setFlightRequestRepo(FlightRequestRepository flightRequestRepo) {
        this.flightRequestRepo = flightRequestRepo;
    }
}
