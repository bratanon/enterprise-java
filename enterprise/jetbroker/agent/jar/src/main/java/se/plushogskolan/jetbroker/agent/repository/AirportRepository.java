package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Airport;

/**
 * Airport repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface AirportRepository {

    /**
     * Gets an airport with the given airport code.
     *
     * @param code
     *            an airport code.
     * @return an airport with the given airport code.
     */
    Airport getAirportByCode(String code);

    /**
     * Gets all airports.
     *
     * @return a list of airports.
     */
    List<Airport> getAllAirports();
}
