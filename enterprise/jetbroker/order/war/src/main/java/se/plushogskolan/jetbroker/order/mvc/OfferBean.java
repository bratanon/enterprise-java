package se.plushogskolan.jetbroker.order.mvc;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import se.plushogskolan.jetbroker.order.domain.Offer;

/**
 * Offer bean.
 *
 * @author Emil Stjerneman
 *
 */
public class OfferBean {

    @NotNull(message = "{validation.offer.price.missing}")
    @Min(value = 1, message = "{validation.offer.price.min}")
    private Integer price;

    @NotNull(message = "{validation.offer.planeId.missing}")
    private Long planeId;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getPlaneId() {
        return planeId;
    }

    public void setPlaneId(Long planeId) {
        this.planeId = planeId;
    }

    public void copyOfferValuesToBean(Offer offer) {
        setPlaneId(offer.getPlane().getId());
        setPrice(offer.getPrice());
    }
}
