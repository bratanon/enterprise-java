package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
@RequestMapping("/flightrequest")
public class FlightRequestController {

    private final static Logger LOG = Logger.getLogger(FlightRequestController.class.getName());

    @Inject
    private FlightRequestService flightRequestService;

    @Inject
    private AirportService airportService;

    @Inject
    private FuelPriceService fuelService;

    @Inject
    private PlaneService planeService;

    @Inject
    private PlaneTypeService planeTypeService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView flightRequest(@PathVariable long id) {
        FlightRequest flightRequest = flightRequestService.getFlightRequest(id);

        OfferBean offerBean = new OfferBean();
        if (flightRequest.getOffer() != null) {
            offerBean.copyOfferValuesToBean(flightRequest.getOffer());
        }

        ModelAndView mav = new ModelAndView("flightRequest");
        mav.addObject("offerBean", offerBean);
        addWrapperDataToMav(mav, flightRequest);
        return mav;
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.POST)
    public ModelAndView offerSubmit(@RequestParam String op, @PathVariable long id, @Valid OfferBean bean, BindingResult errors, Locale locale) {
        FlightRequest flightRequest = flightRequestService.getFlightRequest(id);

        // We don't need any validation for rejects, so lets act before the
        // validation.
        if (op.equals(messageSource.getMessage("offer.button.reject", null, locale))) {
            LOG.fine("Reject button pressed");
            flightRequestService.handleRejectFlightRequest(id);
            return new ModelAndView("redirect:/index.html");
        }

        // Handle errors.
        if (errors.hasErrors()) {
            LOG.fine("Offer has errors");
            ModelAndView mav = new ModelAndView("flightRequest");
            mav.addObject("offerBean", bean);
            addWrapperDataToMav(mav, flightRequest);
            return mav;
        }

        // Create a new offer, or update an existing.
        Offer offer = (flightRequest.getOffer() != null) ? flightRequest.getOffer() : new Offer();
        offer.setPrice(bean.getPrice());
        offer.setPlane(planeService.getPlane(bean.getPlaneId()));
        flightRequestService.handleUpdatedOffer(flightRequest.getId(), offer);

        return new ModelAndView("redirect:/index.html");
    }

    private void addWrapperDataToMav(ModelAndView mav, FlightRequest flightRequest) {
        final FlightRequestWrapper flightRequestWrapper = new FlightRequestWrapper(flightRequest);

        List<PlaneWrapper> planeWrappers = new ArrayList<>();
        for (Plane plane : planeService.getAllPlanes()) {
            planeWrappers.add(new PlaneWrapper(plane, flightRequestWrapper));
        }

        mav.addObject("flightRequestWrapper", flightRequestWrapper);
        mav.addObject("planeWrappers", planeWrappers);
    }

    /**
     * Flight request wrapper class used for presentation.
     *
     * @author Emil Stjerneman
     *
     */
    public class FlightRequestWrapper {

        private FlightRequest flightRequest;
        private Airport arrivalAirport;
        private Airport departureAirport;
        private double distance;
        private double fuelCost;

        public FlightRequestWrapper (FlightRequest flightRequest) {
            setFlightRequest(flightRequest);
            setDepartureAirport(airportService.getAirportByCode(flightRequest.getDepartureAirportCode()));
            setArrivalAirport(airportService.getAirportByCode(flightRequest.getArrivalAirportCode()));

            setDistance(HaversineDistance.getDistance(getDepartureAirport().getLatitude(),
                    getDepartureAirport().getLongitude(),
                    getArrivalAirport().getLatitude(),
                    getArrivalAirport().getLongitude()));

            setFuelCost(fuelService.getFuelPrice());
        }

        public FlightRequest getFlightRequest() {
            return flightRequest;
        }

        public void setFlightRequest(FlightRequest flightRequest) {
            this.flightRequest = flightRequest;
        }

        public Airport getArrivalAirport() {
            return arrivalAirport;
        }

        public void setArrivalAirport(Airport arrivalAirport) {
            this.arrivalAirport = arrivalAirport;
        }

        public Airport getDepartureAirport() {
            return departureAirport;
        }

        public void setDepartureAirport(Airport departureAirport) {
            this.departureAirport = departureAirport;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public double getFuelCost() {
            return fuelCost;
        }

        public void setFuelCost(double fuelCost) {
            this.fuelCost = fuelCost;
        }

    }

    /**
     * Plane wrapper class used for presentation.
     *
     * @author Emil Stjerneman
     *
     */
    public class PlaneWrapper {

        private Plane plane;
        private PlaneType planeType;
        private int totalFuelPrice;
        private double travelTime;

        public PlaneWrapper (Plane plane, FlightRequestWrapper wrapper) {
            setPlane(plane);
            setPlaneType(planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode()));
            setTotalFuelPrice(fuelService.calculateFuelPrice(wrapper.getDistance(), planeType.getFuelConsumptionPerKm()));
            setTravelTime((wrapper.getDistance() / planeType.getMaxSpeedKmH()) * 60);
        }

        public Plane getPlane() {
            return plane;
        }

        public void setPlane(Plane plane) {
            this.plane = plane;
        }

        public PlaneType getPlaneType() {
            return planeType;
        }

        public void setPlaneType(PlaneType planeType) {
            this.planeType = planeType;
        }

        public int getTotalFuelPrice() {
            return totalFuelPrice;
        }

        public void setTotalFuelPrice(int totalFuelPrice) {
            this.totalFuelPrice = totalFuelPrice;
        }

        public double getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(double travelTime) {
            this.travelTime = travelTime;
        }

    }
}
