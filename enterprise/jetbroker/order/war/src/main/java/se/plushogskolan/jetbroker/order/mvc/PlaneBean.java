package se.plushogskolan.jetbroker.order.mvc;

import se.plushogskolan.jetbroker.order.domain.Plane;

/**
 * Plane bean.
 *
 * @author Emil Stjerneman
 *
 */
public class PlaneBean {

    private long id;

    private String code;

    private String planeTypeCode;

    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPlaneTypeCode() {
        return planeTypeCode;
    }

    public void setPlaneTypeCode(String planeTypeCode) {
        this.planeTypeCode = planeTypeCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void copyBeanValuesToPlane(Plane plane) {
        plane.setId(getId());
        plane.setCode(getCode());
        plane.setPlaneTypeCode(getPlaneTypeCode());
        plane.setDescription(getDescription());
    }

    public void copyPlaneValuesToBean(Plane plane) {
        setId(plane.getId());
        setCode(plane.getCode());
        setPlaneTypeCode(plane.getPlaneTypeCode());
        setDescription(plane.getDescription());
    }
}
