package se.plushogskolan.jetbroker.order.mvc;

import java.util.Random;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.integration.FlightRequestMessageSimulator;

@Controller
public class FakeFlightRequestController {

    private final static Logger LOG = Logger.getLogger(FakeFlightRequestController.class.getName());

    @Inject
    FlightRequestMessageSimulator flightRequestMessageSimulator;

    @RequestMapping(value = "/createFakeFlightRequest")
    public void handleRequest(HttpServletResponse response) {
        Random rand = new Random();

        FlightRequest flightRequest = new FlightRequest();
        flightRequest.setAgentId((long) rand.nextInt(500));
        flightRequest.setArrivalAirportCode("GOT");
        flightRequest.setDepartureAirportCode("ARN");
        flightRequest.setDepartureDate(DateTime.now());
        flightRequest.setPassengers(rand.nextInt(500));

        LOG.finest("Created flight request: " + flightRequest.toString());

        flightRequestMessageSimulator.sendMessage(flightRequest);

        // Send 200 back to the client.
        response.setStatus(200);
    }

}
