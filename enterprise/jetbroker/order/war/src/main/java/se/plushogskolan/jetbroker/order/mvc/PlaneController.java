package se.plushogskolan.jetbroker.order.mvc;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
@RequestMapping("/plane")
public class PlaneController {

    private static final Logger LOG = Logger.getLogger(PlaneController.class.getName());

    @Inject
    private PlaneService planeService;

    @Inject
    private PlaneTypeService planeTypeService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView planeForm() {
        return this.planeForm(0);
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public ModelAndView planeForm(@PathVariable long id) {
        PlaneBean bean = new PlaneBean();

        boolean isNew = !(id > 0);
        if (!isNew) {
            bean.copyPlaneValuesToBean(planeService.getPlane(id));
        }

        List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
        Collections.sort(planeTypes);

        ModelAndView mav = new ModelAndView("planeForm");
        mav.addObject("isNew", isNew);
        mav.addObject("planeBean", bean);
        mav.addObject("planeTypes", planeTypes);
        return mav;
    }

    @RequestMapping(value = { "/add", "/{id}/edit" }, method = RequestMethod.POST)
    public ModelAndView handleFlightRequestForm(PlaneBean bean, BindingResult errors) {
        boolean isNew = !(bean.getId() > 0);

        // Handle errors.
        if (errors.hasErrors()) {
            List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
            Collections.sort(planeTypes);

            ModelAndView mav = new ModelAndView("planeForm");
            mav.addObject("isNew", isNew);
            mav.addObject("planeBean", bean);
            mav.addObject("planeTypes", planeTypes);
            return mav;
        }

        if (isNew) {
            Plane plane = new Plane();
            bean.copyBeanValuesToPlane(plane);
            planeService.createPlane(plane);
            LOG.fine("New plane created");
        }
        else {
            Plane plane = planeService.getPlane(bean.getId());
            bean.copyBeanValuesToPlane(plane);
            planeService.updatePlane(plane);
            LOG.fine("Plane updated");
        }

        return new ModelAndView("redirect:/index.html");
    }
}
