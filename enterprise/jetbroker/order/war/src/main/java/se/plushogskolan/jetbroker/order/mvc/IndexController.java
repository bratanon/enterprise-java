package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
public class IndexController {

    @Inject
    private PlaneService planeService;

    @Inject
    private PlaneTypeService planeTypeService;

    @Inject
    private FuelPriceService fuelPriceService;

    @Inject
    private FlightRequestService flightRequestService;

    @Inject
    private AirportService airportService;

    @RequestMapping("/index.html")
    public ModelAndView index() {

        List<FlightRequestWrapper> flightRequestWrappers = new ArrayList<>();
        for (FlightRequest flightRequest : flightRequestService.getAllFlightRequests()) {
            flightRequestWrappers.add(new FlightRequestWrapper(flightRequest));
        }

        List<PlaneWrapper> planeWrappers = new ArrayList<>();
        for (Plane plane : planeService.getAllPlanes()) {
            planeWrappers.add(new PlaneWrapper(plane));
        }

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("flightRequestWrappers", flightRequestWrappers);
        mav.addObject("planeWrappers", planeWrappers);
        mav.addObject("fuelPrice", fuelPriceService.getFuelPrice());
        return mav;
    }

    /**
     * Plane wrapper class used for presentation.
     *
     * @author Emil Stjerneman
     *
     */
    public class PlaneWrapper {

        private Plane plane;
        private PlaneType planeType;

        public PlaneWrapper (Plane plane) {
            setPlane(plane);
            setPlaneType(planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode()));
        }

        public Plane getPlane() {
            return plane;
        }

        public void setPlane(Plane plane) {
            this.plane = plane;
        }

        public PlaneType getPlaneType() {
            return planeType;
        }

        public void setPlaneType(PlaneType planeType) {
            this.planeType = planeType;
        }
    }

    /**
     * Flight request wrapper class used for presentation.
     *
     * @author Emil Stjerneman
     *
     */
    public class FlightRequestWrapper {

        private FlightRequest flightRequest;
        private Airport arrivalAirport;
        private Airport departureAirport;
        private PlaneType planeType;

        public FlightRequestWrapper (FlightRequest flightRequest) {
            setFlightRequest(flightRequest);
            setDepartureAirport(airportService.getAirportByCode(flightRequest.getDepartureAirportCode()));
            setArrivalAirport(airportService.getAirportByCode(flightRequest.getArrivalAirportCode()));

            if (flightRequest.getOffer() != null) {
                setPlaneType(planeTypeService.getPlaneTypeByCode(flightRequest.getOffer().getPlane().getPlaneTypeCode()));
            }
        }

        public FlightRequest getFlightRequest() {
            return flightRequest;
        }

        public void setFlightRequest(FlightRequest flightRequest) {
            this.flightRequest = flightRequest;
        }

        public Airport getArrivalAirport() {
            return arrivalAirport;
        }

        public void setArrivalAirport(Airport arrivalAirport) {
            this.arrivalAirport = arrivalAirport;
        }

        public Airport getDepartureAirport() {
            return departureAirport;
        }

        public void setDepartureAirport(Airport departureAirport) {
            this.departureAirport = departureAirport;
        }

        public PlaneType getPlaneType() {
            return planeType;
        }

        public void setPlaneType(PlaneType planeType) {
            this.planeType = planeType;
        }

    }
}
