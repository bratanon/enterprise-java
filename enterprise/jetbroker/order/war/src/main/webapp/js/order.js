(function($) {
    $(function() {
        $("tr").has("input:radio").click(function(event) {
            if ($(event.target).is("input")) {
              return true;
            }

            $(this).find("input:radio").prop('checked', true);

        });
    });
})(jQuery)