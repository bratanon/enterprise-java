<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<nav id="site-nav">
    <div class="site-wrapper">
        <ul>
            <li><a href="<c:url value="/index.html"/>"class="active"><i class="icon-home"></i><spring:message code="navigation.dashboard"/></a></li>
            <li><a href="<c:url value="/plane/add.html"/>"><i class="icon-flight"></i>New plane</a></li>
        </ul>
        <ul class="languages">
            <li><a href="?lang=sv_SE" class="flag-icon-se" title="<spring:message code="language.swedish"/>"><spring:message code="language.swedish"/></a></li>
            <li><a href="?lang=en_GB" class="flag-icon-gb" title="<spring:message code="language.english"/>"><spring:message code="language.english"/></a></li>
        </ul>
    </div>
</nav>