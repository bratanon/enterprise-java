<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<spring:message code="offer.button.submit" var="submitTitle"/>
<spring:message code="offer.button.reject" var="rejectTitle"/>
<section>
    <h1><spring:message code="offer.title" /></h1>
    <c:if test="${empty planeWrappers}">
        <p><spring:message code="offer.noOffers" /></p>
    </c:if>
    <c:if test="${not empty planeWrappers}">
        <form:form commandName="offerBean" method="post">
        <table>
            <thead>
                <tr>
                    <th class="id"></th>
                    <th><spring:message code="offer.plane" /></th>
                    <th><spring:message code="planeType.passengers" /></th>
                    <th><spring:message code="planeType.maxSpeed" /></th>
                    <th><spring:message code="planeType.fuelConsumption" /></th>
                    <th><spring:message code="offer.totalFuelCost" /></th>
                    <th><spring:message code="offer.travelTime" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${planeWrappers}" var="wrapper">
                    <tr>
                        <td><form:radiobutton path="planeId" value="${wrapper.plane.id}" /></td>
                        <td><c:out value="${wrapper.planeType.name}"/></td>
                        <td><fmt:formatNumber value="${wrapper.planeType.maxNoOfPassengers}"/></td>
                        <td><fmt:formatNumber value="${wrapper.planeType.maxSpeedKmH}"/></td>
                        <td><fmt:formatNumber value="${wrapper.planeType.fuelConsumptionPerKm}" maxFractionDigits="0"/></td>
                        <td><fmt:formatNumber value="${wrapper.totalFuelPrice}" /> SEK</td>
                        <td><fmt:formatNumber value="${wrapper.travelTime}" maxFractionDigits="0"/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <form:errors path="planeId" cssClass="error" />

        <div class="form-item">
            <label for="price"><spring:message code="offer.price" /></label>
            <form:input path="price" />
            <form:errors path="price" cssClass="error" />
        </div>

        <div class="form-actions">
            <input type="submit" name="op" value="${submitTitle}" />
            <input type="submit" name="op" value="${rejectTitle}" />
            <a href="<c:url value="/index.html"/>"><spring:message code="global.cancel" /></a>
        </div>
        </form:form>
    </c:if>
</section>