<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set value="Add new plane" var="title"/>
<c:set value="Create" var="submitTitle"/>
<c:if test="${!isNew}">
    <c:set value="Update plane" var="title"/>
    <c:set value="Update plane" var="submitTitle"/>
</c:if>
                
<!DOCTYPE html>
<html>
<head>
  <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
      <div class="site-wrapper">
        <section class="width50">
          <h1><i class="icon-flight"></i>${title}</h1>

          <form:form commandName="planeBean" method="post">

            <c:if test="${!isNew}">
            <div class="form-item">
                <label>ID</label>
                <c:out value="${planeBean.id}" />
            </div>
            </c:if>

            <div class="form-item">
                <label for="code">Code</label>
                <form:input path="code" />
            </div>

             <div class="form-item">
                <label for="planeTypeCode">Plane type</label>
                <form:select  path="planeTypeCode">
                    <c:if test="${isNew}">
                        <form:option value="" label="-- Select plane type --"/>
                    </c:if>
                    <form:options items="${planeTypes}" itemValue="code" itemLabel="code"/>
                </form:select>
            </div>

            <div class="form-item">
                <label for="description">Description</label>
                <form:textarea path="description" />
            </div>

            <div class="form-actions">
                <input type="submit" name="op" value="${submitTitle}" />
                <a href="<c:url value="/index.html"/>">Cancel</a>
            </div>

            <c:if test="${!isNew}">
                <form:hidden path="id" />
            </c:if>

          </form:form>
        </section>
      </div>
    </div>
</body>

</html>