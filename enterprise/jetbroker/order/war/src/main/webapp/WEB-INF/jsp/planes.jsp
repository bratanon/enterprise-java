<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section>
    <h1><i class="icon-flight"></i>Our planes</h1>
    <table>
        <thead>
            <tr>
                <th class="id">ID</th>
                <th>Plane code</th>
                <th>Type code</th>
                <th>Name</th>
                <th>Max passengers</th>
                <th>Range (km)</th>
                <th>Max speed (km/h)</th>
                <th>Description</th>
                <th class="operations">Operations</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${planeWrappers}" var="wrapper">
                <spring:url value="/plane/${wrapper.plane.id}/edit.html" var="edit" />
                <tr>
                    <td><c:out value="${wrapper.plane.id}"/></td>
                    <td><c:out value="${wrapper.plane.code}"/></td>
                    <td><c:out value="${wrapper.planeType.code}"/></td>
                    <td><c:out value="${wrapper.planeType.name}"/></td>
                    <td><fmt:formatNumber value="${wrapper.planeType.maxNoOfPassengers}"/></td>
                    <td><fmt:formatNumber value="${wrapper.planeType.maxRangeKm}"/></td>
                    <td><fmt:formatNumber value="${wrapper.planeType.maxSpeedKmH}"/></td>
                    <td><c:out value="${wrapper.plane.description}"/></td>
                    <td class="operations align-right">
                        <a href="${edit}" class="edit" title="Edit"><i class="icon-cog"></i></a>
                    </td>
                </tr>
            </c:forEach>
            <c:if test="${empty planeWrappers}">
                <tr><td colspan="8">No planes found.</td></tr>
            </c:if>
        <tbody>
    </table>
</section>
