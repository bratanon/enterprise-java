<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
  <jsp:include page="head.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />
    <jsp:include page="menu.jsp" />
    <div id="main">
      <div class="site-wrapper">
        <section class="width50">
          <h1><i class="icon-inbox"></i><spring:message code="flightRequest.updateTitle"/></h1>
            <table>
                <tbody>
                    <tr>
                        <th><spring:message code="global.id" /></th>
                        <td><c:out value="${flightRequestWrapper.flightRequest.id}" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="flightRequest.departureAirport" /></th>
                        <td><c:out value="${flightRequestWrapper.departureAirport.fullName}" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="flightRequest.arrivalAirport" /></th>
                        <td><c:out value="${flightRequestWrapper.arrivalAirport.fullName}" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="flightRequest.date" /></th>
                        <td><c:out value="${flightRequestWrapper.flightRequest.formatedDate}" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="flightRequest.passengers" /></th>
                        <td><c:out value="${flightRequestWrapper.flightRequest.passengers}" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="flightRequest.distance" /></th>
                        <td><fmt:formatNumber value="${flightRequestWrapper.distance}" maxFractionDigits="0"/></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <tbody>
                    <tr>
                        <th><spring:message code="fuel.fuelCost" /></th>
                        <td><fmt:formatNumber value="${flightRequestWrapper.fuelCost}" /> SEK</td>
                    </tr>
                </tbody>
            </table>
        </section>

        <jsp:include page="offerForm.jsp" />

      </div>
    </div>
</body>

</html>