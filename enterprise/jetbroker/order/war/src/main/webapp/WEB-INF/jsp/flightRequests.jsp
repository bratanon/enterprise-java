<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<section>
    <h1><i class="icon-inbox"></i><spring:message code="flightRequest.title" /></h1>
    <table>
        <thead>
            <tr>
                <th class="id"><spring:message code="global.id" /></th>
                <th><spring:message code="flightRequest.status" /></th>
                <th><spring:message code="flightRequest.departureAirport" /></th>
                <th><spring:message code="flightRequest.arrivalAirport" /></th>
                <th><spring:message code="flightRequest.date" /></th>
                <th><spring:message code="flightRequest.passengers" /></th>
                <th><spring:message code="flightRequest.offeredPrice" /></th>
                <th><spring:message code="flightRequest.offeredPlane" /></th>
                <th class="operations"><spring:message code="global.operations" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${flightRequestWrappers}" var="wrapper">
                <spring:url value="/flightrequest/${wrapper.flightRequest.id}/edit.html" var="edit" />
                <tr>
                    <td><c:out value="${wrapper.flightRequest.id}"/></td>
                    <td><c:out value="${wrapper.flightRequest.status.title}"/></td>
                    <td><abbr title="<c:out value="${wrapper.departureAirport.fullName}"/>"><c:out value="${wrapper.departureAirport.code}"/></abbr></td>
                    <td><abbr title="<c:out value="${wrapper.arrivalAirport.fullName}"/>"><c:out value="${wrapper.arrivalAirport.code}"/></abbr></td>
                    <td><c:out value="${wrapper.flightRequest.formatedDate}"/></td>
                    <td><fmt:formatNumber value="${wrapper.flightRequest.passengers}"/></td>
                    <td>
                        <c:if test="${not empty wrapper.flightRequest.offer.price}">
                            <fmt:formatNumber value="${wrapper.flightRequest.offer.price}"/> SEK
                        </c:if>
                    </td>
                    <td><c:out value="${wrapper.planeType.nameAndPassengerCapacity}"/></td>
                    <td class="operations align-right">
                        <a href="${edit}" class="edit" title="<spring:message code="global.edit" />"><i class="icon-cog"></i></a>
                    </td>
                </tr>
            </c:forEach>
            <c:if test="${empty flightRequestWrappers}">
                <tr><td colspan="9"><spring:message code="flightRequest.noFlightRequests" /></td></tr>
            </c:if>
        <tbody>
    </table>
</section>