package se.plushogskolan.jetbroker.order.service;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Event;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture.FlightRequestBuilder;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmationMessage;
import se.plushogskolan.jetbroker.order.domain.FlightRequestRejectedMessage;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.facade.FlightRequestIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

    private final FlightRequestServiceImpl service = new FlightRequestServiceImpl();

    private FlightRequestRepository repository;

    private FlightRequest flightRequest;

    private FlightRequestIntegrationFacade flightRequestFacade;

    @Before
    public void setUp() {
        repository = EasyMock.createMock(FlightRequestRepository.class);
        flightRequestFacade = EasyMock.createMock(FlightRequestIntegrationFacade.class);
        service.setFlightRequestRepo(repository);
        service.setFlightRequestIntegrationFacade(flightRequestFacade);
        flightRequest = new FlightRequestBuilder().build();
    }

    @Test
    public void createFlightRequest() {
        expect(repository.persist(flightRequest)).andReturn(1L);
        expect(repository.findById(1L)).andReturn(flightRequest);
        replay(repository);

        service.createFlightRequest(flightRequest);

        verify(repository);
    }

    @Test
    public void updateFlightRequest() {
        repository.update(flightRequest);
        replay(repository);

        service.updateFlightRequest(flightRequest);

        verify(repository);
    }

    @Test
    public void getFlightRequest() {
        expect(repository.findById(1)).andReturn(flightRequest);
        replay(repository);

        service.getFlightRequest(1);

        verify(repository);
    }

    @Test
    public void getAllFlightRequests() {
        List<FlightRequest> flightRequests = new ArrayList<>();
        flightRequests.add(flightRequest);
        expect(repository.getAllFlightRequests()).andReturn(flightRequests);
        replay(repository);

        assertEquals("Correct number of flight requests in list", flightRequests.size(), service.getAllFlightRequests().size());

        verify(repository);
    }

    @Test
    public void getAllFlightRequests_withStatus() {
        List<FlightRequest> flightRequests = new ArrayList<>();
        flightRequest.setStatus(Status.NEW);
        flightRequests.add(flightRequest);

        expect(repository.getAllFlightRequests(Status.NEW)).andReturn(flightRequests);
        replay(repository);

        assertEquals("Correct number of flight requests in list", flightRequests.size(), service.getAllFlightRequests(Status.NEW).size());

        verify(repository);
    }

    @Test
    public void handleIncomingFlightRequest() {
        expect(repository.persist(flightRequest)).andReturn(1L);
        expect(repository.findById(1L)).andReturn(flightRequest);
        replay(repository);

        Event<NewFlightRequestEvent> inCommingEvent = EasyMock.createMock(Event.class);
        inCommingEvent.fire(EasyMock.isA(NewFlightRequestEvent.class));
        replay(inCommingEvent);

        flightRequestFacade.sendConfirmationMessage(new FlightRequestConfirmationMessage(flightRequest.getId(), flightRequest.getAgentId()));
        replay(flightRequestFacade);

        service.setInCommingEvent(inCommingEvent);
        service.handleIncomingFlightRequest(flightRequest);

        verify(repository);
        verify(inCommingEvent);
        verify(flightRequestFacade);
    }

    @Test
    public void handleUpdatedOffer() {
        Offer offer = new Offer();
        expect(repository.findById(1)).andReturn(flightRequest);
        repository.update(flightRequest);

        flightRequestFacade.sendUpdateOfferMessage(offer);

        replay(repository);
        replay(flightRequestFacade);

        service.handleUpdatedOffer(1, offer);

        verify(repository);
        verify(flightRequestFacade);
    }

    @Test
    public void rejectFlightRequest() {
        expect(repository.findById(1)).andReturn(flightRequest);
        repository.update(flightRequest);

        flightRequestFacade.sendRejectedMessage(new FlightRequestRejectedMessage(flightRequest.getId(), flightRequest.getAgentId()));

        replay(repository);
        replay(flightRequestFacade);

        service.handleRejectFlightRequest(1);

        verify(repository);
        verify(flightRequestFacade);
    }
}
