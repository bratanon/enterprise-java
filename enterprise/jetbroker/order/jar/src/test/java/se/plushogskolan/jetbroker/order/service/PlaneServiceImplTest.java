package se.plushogskolan.jetbroker.order.service;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture.PlaneBuilder;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;

public class PlaneServiceImplTest {

    private final PlaneServiceImpl service = new PlaneServiceImpl();

    private PlaneRepository repository;

    private Plane plane;

    @Before
    public void setUp() {
        repository = EasyMock.createMock(PlaneRepository.class);
        service.setRepository(repository);

        plane = new PlaneBuilder().code("ABC123").build();

    }

    @Test
    public void createPlane() {
        expect(repository.persist(plane)).andReturn(1L);
        expect(repository.findById(1L)).andReturn(plane);
        replay(repository);

        service.createPlane(plane);

        verify(repository);
    }

    @Test
    public void updatePlane() {
        repository.update(plane);
        expectLastCall();
        replay(repository);

        service.updatePlane(plane);

        verify(repository);
    }

    @Test
    public void getPlane() {
        expect(repository.findById(1)).andReturn(plane);
        replay(repository);

        service.getPlane(1);

        verify(repository);
    }

    @Test
    public void getAllPlanes() {
        List<Plane> planes = new ArrayList<>();
        planes.add(plane);
        expect(repository.getAllPlanes()).andReturn(planes);
        replay(repository);

        assertEquals("Correct number of planes in list", planes.size(), service.getAllPlanes().size());

        verify(repository);
    }

    @Test
    public void getPlaneByCode() {
        expect(repository.getPlaneByCode("ABC123")).andReturn(plane);
        replay(repository);

        service.getPlaneByCode("ABC123");

        verify(repository);
    }
}
