package se.plushogskolan.jetbroker.order.service;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

public class PlaneTypeServiceImplTest {

    private final PlaneTypeServiceImpl service = new PlaneTypeServiceImpl();

    private PlaneTypeRepository repository;

    @Before
    public void setUp() {
        repository = EasyMock.createMock(PlaneTypeRepository.class);
        service.setRepository(repository);
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    public void testGetPlaneTypeByCode() {
        EasyMock.expect(repository.getPlaneTypeByCode("FOO")).andReturn(new TestFixture.PlaneTypeBuilder().code("FOO").build());
        EasyMock.replay(repository);
        assertEquals("Got correct code back", "FOO", service.getPlaneTypeByCode("FOO").getCode());
        EasyMock.verify(repository);
    }

    @Test
    public void testGetPlaneTypeByCode_null() {
        EasyMock.expect(repository.getPlaneTypeByCode("WTF")).andReturn(null);
        EasyMock.replay(repository);
        service.getPlaneTypeByCode("WTF");
        EasyMock.verify(repository);
    }

    @Test
    public void testGetAllPlaneTypes() {
        EasyMock.expect(repository.getAllPlaneTypes()).andReturn(null);
        EasyMock.replay(repository);
        service.getAllPlaneTypes();
        EasyMock.verify(repository);
    }
}
