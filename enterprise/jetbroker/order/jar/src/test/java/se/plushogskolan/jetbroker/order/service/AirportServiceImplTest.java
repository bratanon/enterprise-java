package se.plushogskolan.jetbroker.order.service;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

public class AirportServiceImplTest {

    private final AirportServiceImpl service = new AirportServiceImpl();

    private AirportRepository repository;

    @Before
    public void setUp() {
        repository = EasyMock.createMock(AirportRepository.class);
        service.setRepository(repository);
    }

    @After
    public void tearDown() {
        repository = null;
    }

    @Test
    public void testGetAirportByCode() {
        EasyMock.expect(repository.getAirportByCode("GOT")).andReturn(new TestFixture.AirportBuilder().code("GOT").build());
        EasyMock.replay(repository);
        assertEquals("Got correct code back", "GOT", service.getAirportByCode("GOT").getCode());
        EasyMock.verify(repository);
    }

    @Test
    public void testGetAirportByCode_null() {
        EasyMock.expect(repository.getAirportByCode("WTF")).andReturn(null);
        EasyMock.replay(repository);
        service.getAirportByCode("WTF");
        EasyMock.verify(repository);
    }

    @Test
    public void testGetAllAirports() {
        EasyMock.expect(repository.getAllAirports()).andReturn(null);
        EasyMock.replay(repository);
        service.getAllAirports();
        EasyMock.verify(repository);
    }
}
