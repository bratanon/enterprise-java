package se.plushogskolan.jetbroker.order.service;

import org.junit.Assert;
import org.junit.Test;

public class FuelPriceServiceImplTest {

    private final FuelPriceServiceImpl service = new FuelPriceServiceImpl();

    @Test
    public void calculateFuelPrice() {
        double distance = 100;
        double fuelConsumption = 0.2;
        double fuelPrice = 10;
        service.setFuelPrice(fuelPrice);
        Assert.assertEquals("Fuel price is correctly calculated", 200, service.calculateFuelPrice(distance, fuelConsumption));
    }
}
