package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Plane;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaPlaneRepositoryIntegrationTest extends AbstractRepositoryTest<Plane, JpaPlaneRepository> {

    @Inject
    JpaPlaneRepository repo;

    @Override
    protected JpaPlaneRepository getRepository() {
        return repo;
    }

    @Override
    protected Plane getEntity1() {
        return new TestFixture.PlaneBuilder().build();
    }

    @Override
    protected Plane getEntity2() {
        return new TestFixture.PlaneBuilder().build();
    }

    @Test
    public void testGetAllPlanes() {
        getRepository().persist(getEntity1());
        getRepository().persist(getEntity2());
        assertEquals("List is correct size", 2, getRepository().getAllPlanes().size());
    }

    @Test
    public void testGetPlaneByCode() {
        long id1 = getRepository().persist(new TestFixture.PlaneBuilder().code("FOO").build());
        long id2 = getRepository().persist(new TestFixture.PlaneBuilder().code("BAR").build());

        Plane plane1 = getRepository().getPlaneByCode("FOO");
        Plane plane2 = getRepository().getPlaneByCode("BAR");

        assertEquals("Returned correct plane1", id1, plane1.getId());
        assertEquals("Returned correct plane2", id2, plane2.getId());
    }

    /**
     * Tests that the repository returns null when asking for a plane with a
     * none existing code.
     */
    @Test
    public void testGetPlaneByCode_invalid() {
        assertNull("Returned null when no plane was found with a none existing code", getRepository().getPlaneByCode("FOO"));
    }

    /**
     * Tests that it is impossible to add multiple planes with the same code.
     */
    /*
     * TODO: Implement this if code should be unique. We have to make sure that
     * plains can not be created without code. How do we make this happen with a
     * public no-arg constructor for hibernate?
     */
    // @Test
    // public void testCreateWithSameCode() {
    // getRepository().persist(new
    // TestFixture.ValidPlaneBuilder().code("FOO").build());
    // getRepository().persist(new
    // TestFixture.ValidPlaneBuilder().code("FOO").build());
    // }

}
