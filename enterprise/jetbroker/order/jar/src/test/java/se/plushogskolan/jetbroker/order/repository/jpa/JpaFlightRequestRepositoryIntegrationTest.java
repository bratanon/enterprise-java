package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

    @Inject
    JpaFlightRequestRepository repo;

    @Inject
    PlaneService planeService;

    @Override
    protected JpaFlightRequestRepository getRepository() {
        return repo;
    }

    @Override
    protected FlightRequest getEntity1() {
        return new TestFixture.FlightRequestBuilder().build();
    }

    @Override
    protected FlightRequest getEntity2() {
        return new TestFixture.FlightRequestBuilder().build();
    }

    @Test
    public void testGetAllFlightRequests() {
        getRepository().persist(getEntity1());
        getRepository().persist(getEntity2());
        assertEquals("List is correct size", 2, getRepository().getAllFlightRequests().size());
    }

    @Test
    public void testCreate_withOrder() {
        PlaneType planeType = new TestFixture.PlaneTypeBuilder().build();
        Plane plane = planeService.createPlane(new TestFixture.PlaneBuilder().planeTypeCode(planeType.getCode()).build());

        Offer offer = new Offer();
        offer.setPlane(plane);
        offer.setPrice(200);

        FlightRequest entity1 = getEntity1();
        entity1.setOffer(offer);

        long id = getRepository().persist(entity1);
        assertNotNull("Offer exists.", getRepository().findById(id).getOffer());
    }
}
