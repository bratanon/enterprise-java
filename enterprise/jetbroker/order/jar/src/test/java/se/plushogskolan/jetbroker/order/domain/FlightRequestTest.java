package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FlightRequestTest {

    @Test
    public void testCreateFlightRequest() {
        FlightRequest flightRequest = new FlightRequest();
        assertEquals("Status new is default set.", FlightRequest.Status.NEW, flightRequest.getStatus());
    }
}
