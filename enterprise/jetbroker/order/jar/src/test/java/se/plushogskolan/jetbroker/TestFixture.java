package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

public class TestFixture {

    private static Logger log = Logger.getLogger(TestFixture.class.getName());

    public static Archive<?> createIntegrationTestArchive() {

        MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");

        WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war").addPackages(true, "se.plushogskolan").addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

        war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
        war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
        war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

        log.info("JAR: " + war.toString(true));

        return war;
    }

    /**
     * Plane builder helper.
     *
     * @author Emil Stjerneman
     *
     */
    public static class PlaneBuilder {

        private long id = 0L;
        private String code = "FOO";
        private String plane_type_code = "BAR";
        private String description = "Baz description";

        public PlaneBuilder id(long id) {
            this.id = id;
            return this;
        }

        public PlaneBuilder code(String code) {
            this.code = code;
            return this;
        }

        public PlaneBuilder planeTypeCode(String plane_type_code) {
            this.plane_type_code = plane_type_code;
            return this;
        }

        public PlaneBuilder description(String description) {
            this.description = description;
            return this;
        }

        public Plane build() {
            Plane plane = new Plane();
            plane.setId(id);
            plane.setCode(code);
            plane.setPlaneTypeCode(plane_type_code);
            plane.setDescription(description);
            return plane;
        }
    }

    /**
     * Airport builder helper.
     *
     * @author Emil Stjerneman
     *
     */
    public static class AirportBuilder {

        private String code = "FOO";
        private String name = "BAR";
        private double latitude = 57.667778;
        private double longitude = 12.294444;

        public AirportBuilder id() {
            return this;
        }

        public AirportBuilder code(String code) {
            this.code = code;
            return this;
        }

        public AirportBuilder name(String name) {
            this.name = name;
            return this;
        }

        public AirportBuilder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public AirportBuilder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Airport build() {
            return new Airport(code, name, latitude, longitude);
        }
    }

    /**
     * Plane type builder helper.
     *
     * @author Emil Stjerneman
     *
     */
    public static class PlaneTypeBuilder {

        private String code = "FOO";
        private String name = "BAR";
        private int maxNoOfPassengers = 1;
        private int maxRangeKm = 1;
        private int maxSpeedKmH = 150;
        private double fuelConsumptionPerKm = 1;

        public PlaneTypeBuilder id() {
            return this;
        }

        public PlaneTypeBuilder code(String code) {
            this.code = code;
            return this;
        }

        public PlaneTypeBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PlaneTypeBuilder maxNoOfPassengers(int maxNoOfPassengers) {
            this.maxNoOfPassengers = maxNoOfPassengers;
            return this;
        }

        public PlaneTypeBuilder maxRangeKm(int maxRangeKm) {
            this.maxRangeKm = maxRangeKm;
            return this;
        }

        public PlaneTypeBuilder maxSpeedKmH(int maxSpeedKmH) {
            this.maxSpeedKmH = maxSpeedKmH;
            return this;
        }

        public PlaneTypeBuilder fuelConsumptionPerKm(double fuelConsumptionPerKm) {
            this.fuelConsumptionPerKm = fuelConsumptionPerKm;
            return this;
        }

        public PlaneType build() {
            return new PlaneType(code, name, maxNoOfPassengers, maxRangeKm, maxSpeedKmH, fuelConsumptionPerKm);
        }
    }

    /**
     * Flight request builder helper.
     *
     * @author Emil Stjerneman
     *
     */
    public static class FlightRequestBuilder {

        private long id = 0L;
        private long agentId = 1;
        private String departureAirportCode = "GOT";
        private String arrivalAirportCode = "ARN";
        private DateTime departureDate = DateTime.now();
        private int passengers = 5;
        private Status status = Status.NEW;

        public FlightRequestBuilder () {}

        public FlightRequestBuilder id(long id) {
            this.id = id;
            return this;
        }

        public FlightRequestBuilder agentId(long agentId) {
            this.agentId = agentId;
            return this;
        }

        public FlightRequestBuilder departureAirportCode(String departureAirportCode) {
            this.departureAirportCode = departureAirportCode;
            return this;
        }

        public FlightRequestBuilder arrivalAirportCode(String arrivalAirportCode) {
            this.arrivalAirportCode = arrivalAirportCode;
            return this;
        }

        public FlightRequestBuilder departureDate(DateTime departureDate) {
            this.departureDate = departureDate;
            return this;
        }

        public FlightRequestBuilder passengers(int passengers) {
            this.passengers = passengers;
            return this;
        }

        public FlightRequestBuilder status(Status status) {
            this.status = status;
            return this;
        }

        public FlightRequest build() {
            FlightRequest request = new FlightRequest();
            request.setId(id);
            request.setAgentId(agentId);
            request.setDepartureAirportCode(departureAirportCode);
            request.setArrivalAirportCode(arrivalAirportCode);
            request.setDepartureDate(departureDate);
            request.setPassengers(passengers);
            request.setStatus(status);
            return request;
        }
    }
}
