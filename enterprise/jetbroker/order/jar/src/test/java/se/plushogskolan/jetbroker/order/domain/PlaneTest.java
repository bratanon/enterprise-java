package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class PlaneTest {

    @Test
    public void testEmptyConstructor() {
        Plane plane = new Plane();

        assertEquals("Id is 0", 0, plane.getId());
        assertNull("Code is null", plane.getCode());
        assertNull("Plane type code is null", plane.getPlaneTypeCode());
        assertNull("Description is null", plane.getDescription());
    }

    @Test
    public void test2ParamConstructor() {
        Plane plane = new Plane("FOO", "BAR");

        assertEquals("Id is 0", 0, plane.getId());
        assertEquals("Code is correct", "FOO", plane.getCode());
        assertEquals("Plane type code is correct", "BAR", plane.getPlaneTypeCode());
        assertNull("Description is null", plane.getDescription());
    }

    @Test
    public void test3ParamConstructor() {
        Plane plane = new Plane("FOO", "BAR", "Baz");

        assertEquals("Id is 0", 0, plane.getId());
        assertEquals("Code is correct", "FOO", plane.getCode());
        assertEquals("Plane type code is correct", "BAR", plane.getPlaneTypeCode());
        assertEquals("Description is correct", "Baz", plane.getDescription());
    }

}
