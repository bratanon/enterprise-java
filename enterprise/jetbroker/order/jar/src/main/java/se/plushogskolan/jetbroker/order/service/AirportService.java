package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;

/**
 * Airport service interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface AirportService {

    /**
     * Gets an airport with a given code.
     *
     * @param code
     *            the airport code.
     * @return an airport with the given code or null if no airport is found.
     */
    Airport getAirportByCode(String code);

    /**
     * Gets all airports.
     *
     * @return a list of airports or null if no airports are found.
     */
    List<Airport> getAllAirports();
}
