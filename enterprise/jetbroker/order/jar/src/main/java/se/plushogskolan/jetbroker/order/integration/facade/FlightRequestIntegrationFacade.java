package se.plushogskolan.jetbroker.order.integration.facade;

import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmationMessage;
import se.plushogskolan.jetbroker.order.domain.FlightRequestRejectedMessage;
import se.plushogskolan.jetbroker.order.domain.Offer;

/**
 * Flight request facade to handle integrations.
 *
 * @author Emil Stjerneman
 *
 */
public interface FlightRequestIntegrationFacade {

    public void sendConfirmationMessage(FlightRequestConfirmationMessage message);

    public void sendUpdateOfferMessage(Offer offer);

    public void sendRejectedMessage(FlightRequestRejectedMessage message);

}
