package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

/**
 * The default plane type service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

    @Inject
    private PlaneTypeRepository repository;

    @Override
    public PlaneType getPlaneTypeByCode(String code) {
        PlaneType plane_type = getRepository().getPlaneTypeByCode(code);

        return plane_type;
    }

    @Override
    public List<PlaneType> getAllPlaneTypes() {
        return getRepository().getAllPlaneTypes();
    }

    public PlaneTypeRepository getRepository() {
        return repository;
    }

    public void setRepository(PlaneTypeRepository repository) {
        this.repository = repository;
    }

}
