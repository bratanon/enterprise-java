package se.plushogskolan.jetbroker.order.integration.facade;

import java.util.logging.Logger;

import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmationMessage;
import se.plushogskolan.jetbroker.order.domain.FlightRequestRejectedMessage;
import se.plushogskolan.jetbroker.order.domain.Offer;

/**
 * Mocked implementation of FlightRequestIntegrationFacade.
 *
 * @author Emil Stjerneman
 *
 */
public class MockFlightRequestIntegrationFacade implements FlightRequestIntegrationFacade {

    private static final Logger log = Logger.getLogger(MockFlightRequestIntegrationFacade.class.getName());

    @Override
    public void sendConfirmationMessage(FlightRequestConfirmationMessage message) {
        log.info("sendConfirmationMessage : " + message);
    }

    @Override
    public void sendUpdateOfferMessage(Offer offer) {
        log.info("sendUpdateOfferMessage : " + offer);
    }

    @Override
    public void sendRejectedMessage(FlightRequestRejectedMessage message) {
        log.info("sendRejectedMessage : " + message);
    }

}
