package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

/**
 * Mocked implementation of plane type repository.
 *
 * @author Emil Stjerneman
 *
 */
public class MockPlaneTypeRepository implements PlaneTypeRepository {

    private Map<String, PlaneType> plane_types = new HashMap<String, PlaneType>();

    public MockPlaneTypeRepository () {
        plane_types.put("AIR450", new PlaneType("AIR450", "Airbus 450", 300, 9000, 890, 22));
        plane_types.put("BOE737", new PlaneType("BOE737", "Boeing 373", 250, 740, 930, 19));
        plane_types.put("JAS39", new PlaneType("JAS39", "Jas 39 Gripen", 2, 18000, 1200, 50));
    }

    @Override
    public PlaneType getPlaneTypeByCode(String code) {
        return plane_types.get(code);
    }

    @Override
    public List<PlaneType> getAllPlaneTypes() {
        return new ArrayList<PlaneType>(plane_types.values());
    }

}
