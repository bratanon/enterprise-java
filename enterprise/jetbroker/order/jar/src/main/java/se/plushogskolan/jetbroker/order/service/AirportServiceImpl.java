package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

/**
 * The default airport service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class AirportServiceImpl implements AirportService {

    @Inject
    private AirportRepository repository;

    @Override
    public Airport getAirportByCode(String code) {
        Airport airport = getRepository().getAirportByCode(code);

        return airport;
    }

    @Override
    public List<Airport> getAllAirports() {
        return getRepository().getAllAirports();
    }

    public AirportRepository getRepository() {
        return repository;
    }

    public void setRepository(AirportRepository repository) {
        this.repository = repository;
    }

}
