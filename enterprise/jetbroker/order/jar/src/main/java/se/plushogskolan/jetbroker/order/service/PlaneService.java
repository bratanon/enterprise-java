package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Plane;

/**
 * Plane service interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface PlaneService {

    Plane createPlane(Plane plane);

    Plane getPlane(long id);

    void updatePlane(Plane plane);

    void removePlane(Plane plane);

    Plane getPlaneByCode(String code);

    List<Plane> getAllPlanes();
}
