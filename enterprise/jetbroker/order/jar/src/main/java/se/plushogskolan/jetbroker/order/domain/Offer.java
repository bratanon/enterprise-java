package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Offers are bounded to flight requests and contains only metadata for offers.
 *
 * @author Emil Stjerneman
 *
 */
@Entity
public class Offer implements IdHolder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int price;

    @ManyToOne
    @JoinColumn(name = "planeId")
    @NotNull
    private Plane plane;

    @OneToOne(mappedBy = "offer")
    private FlightRequest flightRequest;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Plane getPlane() {
        return plane;
    }

    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    public FlightRequest getFlightRequest() {
        return flightRequest;
    }

    public void setFlightRequest(FlightRequest flightRequest) {
        this.flightRequest = flightRequest;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((flightRequest == null) ? 0 : flightRequest.hashCode());
        result = prime * result + ((plane == null) ? 0 : plane.hashCode());
        result = prime * result + price;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Offer)) {
            return false;
        }
        Offer other = (Offer) obj;
        if (flightRequest == null) {
            if (other.flightRequest != null) {
                return false;
            }
        }
        else if (!flightRequest.equals(other.flightRequest)) {
            return false;
        }
        if (plane == null) {
            if (other.plane != null) {
                return false;
            }
        }
        else if (!plane.equals(other.plane)) {
            return false;
        }
        if (price != other.price) {
            return false;
        }
        return true;
    }

}
