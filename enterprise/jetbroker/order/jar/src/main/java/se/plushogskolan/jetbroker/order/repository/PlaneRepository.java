package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.Plane;

/**
 * Plane repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface PlaneRepository extends BaseRepository<Plane> {

    /**
     * Gets a plane with the given plane code.
     *
     * @param code
     *            a plane code.
     * @return an plane with the given plane code.
     */
    Plane getPlaneByCode(String code);

    /**
     * Gets all planes.
     *
     * @return a list of planes.
     */
    List<Plane> getAllPlanes();
}
