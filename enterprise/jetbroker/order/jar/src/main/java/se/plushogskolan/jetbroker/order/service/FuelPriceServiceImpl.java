package se.plushogskolan.jetbroker.order.service;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * The default fuel price service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Singleton
@Startup
public class FuelPriceServiceImpl implements FuelPriceService {

    /**
     * Temporary mocked value.
     */
    private double fuelPrice;

    @PostConstruct
    private void init() {
        setFuelPrice(5.2);
    }

    @Override
    public double getFuelPrice() {
        return this.fuelPrice;
    }

    @Override
    public void setFuelPrice(double fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    @Override
    public int calculateFuelPrice(double distance, double fuelConsumption) {
        return (int) ((fuelConsumption * distance) * getFuelPrice());
    }

}
