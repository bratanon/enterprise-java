package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;

/**
 * The default plane service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class PlaneServiceImpl implements PlaneService {

    @Inject
    private PlaneRepository repository;

    @Override
    public Plane createPlane(Plane plane) {
        long id = getRepository().persist(plane);
        return getPlane(id);
    }

    @Override
    public Plane getPlane(long id) {
        Plane plane = getRepository().findById(id);
        return plane;
    }

    @Override
    public void updatePlane(Plane plane) {
        getRepository().update(plane);
    }

    @Override
    public void removePlane(Plane plane) {
        getRepository().remove(plane);
    }

    @Override
    public Plane getPlaneByCode(String code) {
        Plane plane = getRepository().getPlaneByCode(code);

        return plane;
    }

    @Override
    public List<Plane> getAllPlanes() {
        return getRepository().getAllPlanes();
    }

    public PlaneRepository getRepository() {
        return repository;
    }

    public void setRepository(PlaneRepository repository) {
        this.repository = repository;
    }

}
