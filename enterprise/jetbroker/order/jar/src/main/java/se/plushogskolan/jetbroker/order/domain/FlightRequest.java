package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Class representing a flight request entity.
 *
 * @author Emil Stjerneman
 *
 */
@Entity
public class FlightRequest implements IdHolder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private Long agentId;

    @NotBlank
    private String departureAirportCode;

    @NotBlank
    private String arrivalAirportCode;

    @NotNull
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime departureDate;

    @Range(min = 1, max = 500)
    private int passengers;

    @NotNull
    private Status status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "offerId")
    private Offer offer;

    public FlightRequest () {
        setStatus(Status.NEW);
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public DateTime getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateTime departureDate) {
        this.departureDate = departureDate;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public String getFormatedDate() {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("YYYY-MMM-dd HH:mm");
        return dateFormatter.print(getDepartureDate());
    }

    public enum Status {
        NEW("New"), OFFER_SENT("Offer sent"), REJECTED("Rejected");

        private String title;

        Status (String title) {
            this.title = title;
        }

        public String getTitle() {
            return this.title;
        }
    }

    @Override
    public String toString() {
        return String.format("FlightRequest [id=%s, agentId=%s, departureAirportCode=%s, arrivalAirportCode=%s, departureDate=%s, passengers=%s, status=%s, offer=%s]", id, agentId,
                departureAirportCode, arrivalAirportCode, departureDate, passengers, status, offer);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((agentId == null) ? 0 : agentId.hashCode());
        result = prime * result + ((arrivalAirportCode == null) ? 0 : arrivalAirportCode.hashCode());
        result = prime * result + ((departureAirportCode == null) ? 0 : departureAirportCode.hashCode());
        result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
        result = prime * result + ((offer == null) ? 0 : offer.hashCode());
        result = prime * result + passengers;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FlightRequest)) {
            return false;
        }
        FlightRequest other = (FlightRequest) obj;
        if (agentId == null) {
            if (other.agentId != null) {
                return false;
            }
        }
        else if (!agentId.equals(other.agentId)) {
            return false;
        }
        if (arrivalAirportCode == null) {
            if (other.arrivalAirportCode != null) {
                return false;
            }
        }
        else if (!arrivalAirportCode.equals(other.arrivalAirportCode)) {
            return false;
        }
        if (departureAirportCode == null) {
            if (other.departureAirportCode != null) {
                return false;
            }
        }
        else if (!departureAirportCode.equals(other.departureAirportCode)) {
            return false;
        }
        if (departureDate == null) {
            if (other.departureDate != null) {
                return false;
            }
        }
        else if (!departureDate.equals(other.departureDate)) {
            return false;
        }
        if (offer == null) {
            if (other.offer != null) {
                return false;
            }
        }
        else if (!offer.equals(other.offer)) {
            return false;
        }
        if (passengers != other.passengers) {
            return false;
        }
        return true;
    }

}
