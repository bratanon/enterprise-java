package se.plushogskolan.jetbroker.order.util;

import java.util.logging.Logger;

import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.integration.Prod;
import se.plushogskolan.jetbroker.order.event.NewFlightRequestEvent;

public class FlightRequestLogger {

    private final static Logger LOG = Logger.getLogger(FlightRequestLogger.class.getName());

    private static int calls;

    public void printLog(@Observes @Prod NewFlightRequestEvent event) {
        calls++;

        LOG.info("*** FlightRequestLogger ***");
        LOG.info(String.format("No of incomming flight requests: %d", calls));
        LOG.info("***************************");
    }
}
