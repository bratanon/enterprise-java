package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

/**
 * Plane type repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface PlaneTypeRepository {

    /**
     * Gets a plane type with the given plane type code.
     *
     * @param code
     *            an plane type code.
     * @return a plane type with the given plane type code.
     */
    PlaneType getPlaneTypeByCode(String code);

    /**
     * Gets all plane types.
     *
     * @return a list if plane types.
     */
    List<PlaneType> getAllPlaneTypes();
}
