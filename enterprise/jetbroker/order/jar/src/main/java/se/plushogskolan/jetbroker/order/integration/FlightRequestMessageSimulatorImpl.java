package se.plushogskolan.jetbroker.order.integration;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.integration.Mock;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.mdb.FlightRequestMdb;

@Stateless
public class FlightRequestMessageSimulatorImpl implements FlightRequestMessageSimulator {

    private final static Logger LOG = Logger.getLogger(FlightRequestMessageSimulatorImpl.class.getName());

    @Inject
    FlightRequestMdb mdb;

    @Inject
    @Mock
    Event<NewFlightRequestEvent> inCommingEvent;

    @Override
    public void sendMessage(FlightRequest flightRequest) {
        LOG.finest("Simulator: Sending Message");
        // mdb.onMessage(flightRequest);
        inCommingEvent.fire(new NewFlightRequestEvent(flightRequest));
    }
}
