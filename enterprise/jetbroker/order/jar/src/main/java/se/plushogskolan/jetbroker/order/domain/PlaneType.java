package se.plushogskolan.jetbroker.order.domain;

/**
 * Class representing a plane type entity.
 *
 * @author Emil Stjerneman
 *
 */
public class PlaneType implements Comparable<PlaneType> {

    private String code;
    private String name;
    private int maxNoOfPassengers;
    private int maxRangeKm;
    private int maxSpeedKmH;
    private double fuelConsumptionPerKm;

    public PlaneType () {}

    public PlaneType (String code, String name, int maxNoOfPassengers, int maxRangeKm, int maxSpeedKmH, double fuelConsumptionPerKm) {
        setCode(code);
        setName(name);
        setMaxNoOfPassengers(maxNoOfPassengers);
        setMaxRangeKm(maxRangeKm);
        setMaxSpeedKmH(maxSpeedKmH);
        setFuelConsumptionPerKm(fuelConsumptionPerKm);
    }

    public String getCode() {
        return code;
    }

    private void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public int getMaxNoOfPassengers() {
        return maxNoOfPassengers;
    }

    private void setMaxNoOfPassengers(int maxNoOfPassengers) {
        this.maxNoOfPassengers = maxNoOfPassengers;
    }

    public int getMaxRangeKm() {
        return maxRangeKm;
    }

    private void setMaxRangeKm(int maxRangeKm) {
        this.maxRangeKm = maxRangeKm;
    }

    public int getMaxSpeedKmH() {
        return maxSpeedKmH;
    }

    private void setMaxSpeedKmH(int maxSpeedKmH) {
        this.maxSpeedKmH = maxSpeedKmH;
    }

    public double getFuelConsumptionPerKm() {
        return fuelConsumptionPerKm;
    }

    private void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
        this.fuelConsumptionPerKm = fuelConsumptionPerKm;
    }

    /**
     * Concatenates the plane type name and the number of passengers.
     *
     * @return a string formated as <code>"%s (%s passengers)"</code>
     */
    public String getNameAndPassengerCapacity() {
        return String.format("%s (%s seats)", getName(), getMaxNoOfPassengers());
    }

    @Override
    public int compareTo(PlaneType o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    @Override
    public String toString() {
        return "PlaneType [code=" + code + ", name=" + name + ", maxNoOfPassengers=" + maxNoOfPassengers + ", maxRangeKm=" + maxRangeKm + ", maxSpeedKmH=" + maxSpeedKmH + ", fuelConsumptionPerKm=" + fuelConsumptionPerKm + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PlaneType other = (PlaneType) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        }
        else if (!code.equals(other.code)) {
            return false;
        }
        return true;
    }

}
