package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

/**
 * Plane type service interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface PlaneTypeService {

    PlaneType getPlaneTypeByCode(String code);

    List<PlaneType> getAllPlaneTypes();
}
