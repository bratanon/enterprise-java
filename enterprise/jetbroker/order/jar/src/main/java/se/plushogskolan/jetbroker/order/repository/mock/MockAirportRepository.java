package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

/**
 * Mocked implementation of airport repository.
 *
 * @author Emil Stjerneman
 *
 */
public class MockAirportRepository implements AirportRepository {

    private Map<String, Airport> airports = new HashMap<String, Airport>();

    public MockAirportRepository () {
        airports.put("GOT", new Airport("GOT", "Göteborg - Landvetter", 57.667778, 12.294444));
        airports.put("GSE", new Airport("GSE", "Göteborg - City Airport", 57.774722, 11.870278));
        airports.put("ARN", new Airport("ARN", "Stockholm - Arlanda", 59.651944, 17.918611));
    }

    @Override
    public Airport getAirportByCode(String code) {
        return airports.get(code);
    }

    @Override
    public List<Airport> getAllAirports() {
        return new ArrayList<Airport>(airports.values());
    }

}
