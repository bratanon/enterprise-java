package se.plushogskolan.jetbroker.order.domain;

/**
 * Class representing an airport.
 *
 * @author Emil Stjerneman
 *
 */
public class Airport implements Comparable<Airport> {

    private String code;
    private String name;
    private double latitude;
    private double longitude;

    public Airport () {}

    public Airport (String code, String name, double latitude, double longitude) {
        setCode(code);
        setName(name);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getFullName() {
        return String.format("%s (%s)", getName(), getCode().toUpperCase());
    }

    @Override
    public int compareTo(Airport o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    @Override
    public String toString() {
        return "Airport [code=" + code + ", name=" + name + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Airport other = (Airport) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        }
        else if (!code.equals(other.code)) {
            return false;
        }
        return true;
    }

}
