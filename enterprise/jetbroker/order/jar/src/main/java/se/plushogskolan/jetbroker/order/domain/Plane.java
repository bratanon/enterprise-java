package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Class representing a plane entity.
 *
 * @author Emil Stjerneman
 *
 */
@Entity
public class Plane implements IdHolder, Comparable<Plane> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    // TODO: Fix so this field is unique.
    // @Column(unique=true)
    private String code;

    @NotBlank
    private String plane_type_code;

    // TODO: Fix sanitation for this field to prevent XSS and HTML input.
    private String description;

    public Plane () {}

    public Plane (String code, String plane_type_code) {
        this(code, plane_type_code, null);
    }

    public Plane (String code, String plane_type_code, String description) {
        setCode(code);
        setPlaneTypeCode(plane_type_code);
        setDescription(description);
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPlaneTypeCode() {
        return plane_type_code;
    }

    public void setPlaneTypeCode(String plane_type_code) {
        this.plane_type_code = plane_type_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Plane o) {
        return getCode().compareTo(o.getCode());
    }

    @Override
    public String toString() {
        return "Plane [id=" + id + ", code=" + code + ", plane_type_code=" + plane_type_code + ", description=" + description + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Plane)) {
            return false;
        }
        Plane other = (Plane) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        }
        else if (!code.equals(other.code)) {
            return false;
        }
        return true;
    }

}
