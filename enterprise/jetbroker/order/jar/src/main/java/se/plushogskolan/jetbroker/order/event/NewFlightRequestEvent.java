package se.plushogskolan.jetbroker.order.event;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public class NewFlightRequestEvent {

    private FlightRequest flightRequest;

    public NewFlightRequestEvent (FlightRequest flightRequest) {
        this.flightRequest = flightRequest;
    }

    public FlightRequest getFlightRequest() {
        return flightRequest;
    }

    public void setFlightRequest(FlightRequest flightRequest) {
        this.flightRequest = flightRequest;
    }

}
