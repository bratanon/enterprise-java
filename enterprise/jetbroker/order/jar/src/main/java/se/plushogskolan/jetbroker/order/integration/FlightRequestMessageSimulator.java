package se.plushogskolan.jetbroker.order.integration;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

/**
 * Simulator interface. This is only needed to CDI.
 *
 * @author Emil Stjerneman
 *
 */
public interface FlightRequestMessageSimulator {

    /**
     * Sends an event to the fake MDB.
     */
    public void sendMessage(FlightRequest flightrequest);
}
