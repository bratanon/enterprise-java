package se.plushogskolan.jetbroker.order.domain;

/**
 * A reject message.
 *
 * @author Emil Stjerneman
 *
 */
public class FlightRequestRejectedMessage {

    private long id;
    private long agentId;

    public FlightRequestRejectedMessage (long id, long agentId) {
        setId(id);
        setAgentId(agentId);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (agentId ^ (agentId >>> 32));
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FlightRequestRejectedMessage)) {
            return false;
        }
        FlightRequestRejectedMessage other = (FlightRequestRejectedMessage) obj;
        if (agentId != other.agentId) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        return true;
    }

}
