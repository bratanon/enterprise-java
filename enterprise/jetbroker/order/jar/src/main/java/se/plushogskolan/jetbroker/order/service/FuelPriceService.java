package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Lock;
import javax.ejb.LockType;

/**
 * Fuel price service interface.
 *
 * Implementations of this interface should be a singleton.
 *
 * @author Emil Stjerneman
 *
 */
public interface FuelPriceService {

    /**
     * Gets the fuel price.
     *
     * @return the fuel price.
     */
    @Lock(LockType.READ)
    double getFuelPrice();

    /**
     * Sets the fuel price.
     *
     * @param fuelCost
     *            the fuel price;
     */
    @Lock(LockType.WRITE)
    void setFuelPrice(double fuelCost);

    /**
     * Calculates the fuel cost for a given distance and fuel consumption.
     *
     * @param distance
     *            the distance in km
     * @param fuelConsumption
     *            the fuel consumption per km
     * @return the total fuel cost for the given distance with the given fuel
     *         consumption.
     */
    int calculateFuelPrice(double distance, double fuelConsumption);
}
