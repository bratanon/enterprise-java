package se.plushogskolan.jetbroker.order.domain;

/**
 * A simple confirmation message.
 *
 * @author Emil Stjerneman
 *
 */
public class FlightRequestConfirmationMessage {

    private long confirmationId;
    private long agentId;

    public FlightRequestConfirmationMessage (long confirmationId, long agentId) {
        setConfirmationId(confirmationId);
        setAgentId(agentId);
    }

    public long getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(long confirmationId) {
        this.confirmationId = confirmationId;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (agentId ^ (agentId >>> 32));
        result = prime * result + (int) (confirmationId ^ (confirmationId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof FlightRequestConfirmationMessage)) {
            return false;
        }
        FlightRequestConfirmationMessage other = (FlightRequestConfirmationMessage) obj;
        if (agentId != other.agentId) {
            return false;
        }
        if (confirmationId != other.confirmationId) {
            return false;
        }
        return true;
    }

}
