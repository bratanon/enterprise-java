package se.plushogskolan.jetbroker.order.integration.mdb;

import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.integration.Mock;
import se.plushogskolan.jetbroker.order.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

/**
 * Fake MDB.
 *
 * @author Emil Stjerneman
 *
 */
public class FlightRequestMdb {

    private final static Logger log = Logger.getLogger(FlightRequestMdb.class.getName());

    @Inject
    FlightRequestService flightRequestService;

    public void onMessage(@Observes @Mock NewFlightRequestEvent inCommingEvent) {
        log.finest("MDB : onMessage called");
        flightRequestService.handleIncomingFlightRequest(inCommingEvent.getFlightRequest());
    }
}
