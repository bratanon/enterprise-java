package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;

/**
 * Airport repository interface.
 *
 * @author Emil Stjerneman
 *
 */
public interface AirportRepository {

    /**
     * Gets an airport with the given airport code.
     *
     * @param code
     *            an airport code.
     * @return an airport with the given airport code.
     */
    Airport getAirportByCode(String code);

    /**
     * Gets all airports.
     *
     * @return a list all airports.
     */
    List<Airport> getAllAirports();
}
