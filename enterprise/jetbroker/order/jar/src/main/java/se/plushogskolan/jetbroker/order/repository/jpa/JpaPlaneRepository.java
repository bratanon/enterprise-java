package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;

/**
 * This class is used to get plane entities using JPA.
 *
 * @author Emil Stjerneman
 *
 */
public class JpaPlaneRepository extends JpaRepository<Plane> implements PlaneRepository {

    @SuppressWarnings("unchecked")
    @Override
    public Plane getPlaneByCode(String code) {
        Query query = em.createQuery("SELECT p FROM Plane p WHERE p.code = :code");
        query.setParameter("code", code);
        List<Plane> resultList = query.getResultList();
        if (resultList.size() == 1) {
            return resultList.get(0);
        }
        else if (resultList.isEmpty()) {
            return null;
        }
        else {
            throw new RuntimeException("Received more then one plane with code " + code + ". This should not be possible.");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Plane> getAllPlanes() {
        return em.createQuery("SELECT p FROM Plane p").getResultList();
    }

}
