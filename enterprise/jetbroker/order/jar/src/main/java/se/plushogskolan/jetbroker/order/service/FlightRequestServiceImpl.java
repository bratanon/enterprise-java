package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.integration.Prod;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmationMessage;
import se.plushogskolan.jetbroker.order.domain.FlightRequestRejectedMessage;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.facade.FlightRequestIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

/**
 * The default flight request service implementation.
 *
 * @author Emil Stjerneman
 *
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

    @Inject
    private FlightRequestRepository flightRequestRepo;

    @Inject
    private FlightRequestIntegrationFacade flightRequestFacade;

    @Inject
    @Prod
    private Event<NewFlightRequestEvent> inCommingEvent;

    @Override
    public FlightRequest createFlightRequest(FlightRequest flightRequest) {
        flightRequest.setStatus(Status.NEW);
        long id = getFlightRequestRepo().persist(flightRequest);
        return getFlightRequest(id);
    }

    @Override
    public void updateFlightRequest(FlightRequest flightRequest) {
        getFlightRequestRepo().update(flightRequest);
    }

    @Override
    public FlightRequest getFlightRequest(long id) {
        return getFlightRequestRepo().findById(id);
    }

    @Override
    public List<FlightRequest> getAllFlightRequests() {
        return getFlightRequestRepo().getAllFlightRequests();
    }

    @Override
    public List<FlightRequest> getAllFlightRequests(Status status) {
        return getFlightRequestRepo().getAllFlightRequests(status);
    }

    @Override
    public void removeFlightRequest(long id) {
        FlightRequest flightRequest = getFlightRequest(id);
        getFlightRequestRepo().remove(flightRequest);
    }

    @Override
    public void handleIncomingFlightRequest(FlightRequest flightRequest) {
        createFlightRequest(flightRequest);

        inCommingEvent.fire(new NewFlightRequestEvent(flightRequest));

        // Send integration message.
        flightRequestFacade.sendConfirmationMessage(new FlightRequestConfirmationMessage(flightRequest.getId(), flightRequest.getAgentId()));

    }

    @Override
    public void handleUpdatedOffer(long id, Offer offer) {
        FlightRequest flightRequest = getFlightRequest(id);
        flightRequest.setOffer(offer);
        flightRequest.setStatus(Status.OFFER_SENT);
        updateFlightRequest(flightRequest);

        // Send offer update message.
        flightRequestFacade.sendUpdateOfferMessage(offer);
    }

    @Override
    public void handleRejectFlightRequest(long id) {
        FlightRequest flightRequest = getFlightRequest(id);
        flightRequest.setOffer(null);
        flightRequest.setStatus(Status.REJECTED);

        updateFlightRequest(flightRequest);

        // Send rejected message.
        flightRequestFacade.sendRejectedMessage(new FlightRequestRejectedMessage(flightRequest.getId(), flightRequest.getAgentId()));
    }

    public FlightRequestRepository getFlightRequestRepo() {
        return flightRequestRepo;
    }

    public void setFlightRequestRepo(FlightRequestRepository flightRequestRepo) {
        this.flightRequestRepo = flightRequestRepo;
    }

    public FlightRequestIntegrationFacade getFlightRequestIntegrationFacade() {
        return flightRequestFacade;
    }

    public void setFlightRequestIntegrationFacade(FlightRequestIntegrationFacade flightRequestFacade) {
        this.flightRequestFacade = flightRequestFacade;
    }

    public Event<NewFlightRequestEvent> getInCommingEvent() {
        return inCommingEvent;
    }

    public void setInCommingEvent(Event<NewFlightRequestEvent> inCommingEvent) {
        this.inCommingEvent = inCommingEvent;
    }

}
